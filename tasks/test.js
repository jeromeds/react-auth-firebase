var gulp = require('gulp');
var spawn = require('child_process').spawn;

module.exports = function (callback) {
  spawn('npm', ['test'], { stdio: 'inherit' }).on('exit', function (code) {
    if (code) return callback(new Error('exit code: ' + code));
    return callback();
  });
};
