var gulp = require('gulp');
var gutil = require('gulp-util');
var merge = require('merge-stream');
var rename = require('gulp-rename');
var argv = require('yargs').argv;

module.exports = function () {
  return merge(
    gulp
      .src(argv.optimize ?
        'node_modules/babel-core/browser-polyfill.min.js' :
        'node_modules/babel-core/browser-polyfill.js'
      )
      .pipe(rename('babel-polyfill.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/systemjs/dist/system.js' :
        'node_modules/systemjs/dist/system.src.js'
      )
      .pipe(rename('system.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src('node_modules/firebase/lib/firebase-web.js')
      .pipe(rename('firebase.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/react/dist/react.min.js' :
        'node_modules/react/dist/react.js'
      )
      .pipe(rename('react.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/react-dom/dist/react-dom.min.js' :
        'node_modules/react-dom/dist/react-dom.js'
      )
      .pipe(rename('react-dom.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/react-router/umd/ReactRouter.min.js' :
        'node_modules/react-router/umd/ReactRouter.js'
      )
      .pipe(rename('react-router.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/history/umd/History.min.js' :
        'node_modules/history/umd/History.js'
      )
      .pipe(rename('history.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/moment/min/moment.min.js' :
        'node_modules/moment/moment.js'
      )
      .pipe(rename('moment.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/react-datepicker/dist/react-datepicker.min.js' :
        'node_modules/react-datepicker/dist/react-datepicker.min.js'
      )
      .pipe(rename('react-datepicker.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/tether/dist/js/tether.min.js' :
        'node_modules/tether/dist/js/tether.js'
      )
      .pipe(rename('tether.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src('node_modules/react-onclickoutside/index.js')
      .pipe(rename('react-onclickoutside.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/flux/dist/Flux.min.js' :
        'node_modules/flux/dist/Flux.js'
      )
      .pipe(rename('flux.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/react-dnd/dist/ReactDnD.min.js' :
        'node_modules/react-dnd/dist/ReactDnD.min.js'
      )
      .pipe(rename('react-dnd.js'))
      .pipe(gulp.dest('public/vendor')),
    gulp
      .src(argv.optimize ?
        'node_modules/react-dnd-html5-backend/dist/ReactDnDHTML5Backend.min.js' :
        'node_modules/react-dnd-html5-backend/dist/ReactDnDHTML5Backend.min.js'
      )
      .pipe(rename('react-dnd-html5-backend.js'))
      .pipe(gulp.dest('public/vendor'))
  );
};
