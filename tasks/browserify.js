var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('./plumber');
var cache = require('gulp-cached');
var map = require('map-stream');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var browserify = require('browserify');
var uglify = require('gulp-uglify');
var argv = require('yargs').argv;

module.exports = function () {
  return gulp.src('src/browserified.js')
    .pipe(plumber())
    .pipe(cache('browserify'))
    .pipe(map(function (file, done) {
      var b = browserify({
        entries: 'src/browserified.js',
        debug: argv.debug,
        standalone: 'browserified'
      });
      b.bundle()
        .on('error', done)
        .pipe(source('browserified.js'))
        .pipe(plumber())
        .pipe(buffer())
        .pipe(argv.optimize ? uglify() : gutil.noop())
        .pipe(gulp.dest('public'))
        .on('end', function () {
          done(null, file);
        });
    }));
};
