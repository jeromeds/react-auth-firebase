var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('./plumber');
var cache = require('gulp-cached');
var sourcemaps = require('gulp-sourcemaps');
var replace = require('gulp-replace');
var babel = require('gulp-babel');
var uglify = require('gulp-uglify');
var argv = require('yargs').argv;
var pkg = require('../package.json');
var fs = require('fs');


function interpolateVersions() {
  return replace(/\$VERSION\(([\w\.\-_]+)\)/g, function (match, key) {
    if (key === pkg.name) return pkg.version;
    return require(key + '/package.json').version;
  });
}

function interpolateConfig() {
  return replace(/CONFIG\.(\w+)/g, function (match, key) {
    return key in argv ? '"' + argv[key] + '"' : 'undefined';
  });
}

function interpolateEnv() {
  return replace(/process\.env\.(\w+)/g, function (match, key) {
    return key in process.env ? '"' + process.env[key] + '"' : 'undefined';
  });
}

module.exports = ['browserify', function () {
  return gulp.src([
    'src/**/*.js',
    // exclude config (will be merged via the `config` task)
    '!src/config.json',
    // exclude modules bundled with browserify
    '!src/browserified.js',
    // exclude tests
    '!src/__tests__/**/*'
  ])
    .pipe(plumber())
    .pipe(cache('app_scripts'))
    .pipe(argv.debug ? sourcemaps.init() : gutil.noop())
    .pipe(interpolateVersions())
    .pipe(interpolateConfig())
    .pipe(interpolateEnv())
    .pipe(babel())
    .pipe(argv.optimize ? uglify() : gutil.noop())
    .pipe(argv.debug ? sourcemaps.write() : gutil.noop())
    .pipe(gulp.dest('public'));
}];
