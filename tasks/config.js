var gulp = require('gulp');
var gutil = require('gulp-util');
var extend = require('gulp-extend');
var argv = require('yargs').argv;

module.exports = function () {
  var env = process.env.NODE_ENV || 'development';
  return gulp.src([
    'config/*.json',
    'config/' + env + '/*.json'
  ])
    .pipe(extend('config.json', true, argv.optimize ? undefined : 2))
    .pipe(gulp.dest('public'));
};
