var gulp = require('gulp');
var livereload = require('gulp-livereload');

module.exports = ['build', function () {

  // Start LR server
  livereload.listen();

  // Compile/copy changed files
  gulp.watch('src/**/*.js', ['app', 'test']);
  gulp.watch('src/**/locales/*.json', ['localization']);
  gulp.watch('src/node_modules.js', ['node_modules']);
  gulp.watch(['stylesheets/**/*', 'src/**/*.scss'], ['stylesheets']);
  gulp.watch('assets/**/*', ['assets']);
  gulp.watch('config/**/*', ['config']);

  // Reload changed files
  gulp.watch('public/**', livereload.changed);

}];
