var gulp = require('gulp');
var gutil = require('gulp-util');
var merge = require('merge-stream');
var replace = require('gulp-replace');
var htmlmin = require('gulp-html-minifier');
var argv = require('yargs').argv;
var pkg = require('../package.json');

var conditionalContent = /\{\{CONFIG\.(\w+)\:([^]+?)\}\}/gm;

var htmlminOptions = {
  collapseWhitespace: true,
  removeAttributeQuotes: true,
  removeRedundantAttributes: true,
  removeEmptyAttributes: true,
  minifyJS: true,
  minifyCSS: true
};

module.exports = function () {
  return merge(
    gulp.src(['assets/**/*', '!assets/**/*.html'])
      .pipe(gulp.dest('public')),
    gulp.src('assets/**/*.html')
      .pipe(replace(/\$VERSION\(([\w\.\-_]+)\)/g, function (match, key) {
        if (key === pkg.name) return pkg.version;
        return require(key + '/package.json').version;
      }))
      .pipe(replace(conditionalContent, function (match, key, content) {
        return argv[key] ? content : '';
      }))
      .pipe(argv.optimize ? htmlmin(htmlminOptions) : gutil.noop())
      .pipe(gulp.dest('public'))
  );
};
