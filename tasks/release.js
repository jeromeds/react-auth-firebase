var gulp = require('gulp');
var git = require('gulp-git');
var bump = require('gulp-bump');
var filter = require('gulp-filter');
var tag = require('gulp-tag-version');
var argv = require('yargs').argv;

module.exports = function () {
  return gulp.src('package.json')
    .pipe(bump({ type: argv.version || 'patch' }))
    .pipe(gulp.dest('./'))
    .pipe(git.commit('bump package version'))
    .pipe(filter('package.json'))
    .pipe(tag());
};
