var SLACK_WEBHOOK_URL = 'https://hooks.slack.com/services/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';
var APP_DEPLOY_URL = 'https://liveshout-app.firebaseapp.com';

var gulp = require('gulp');
var sequence = require('run-sequence');
var Slack = require('node-slack');

function sendNotification(callback) {
  var slack = new Slack(SLACK_WEBHOOK_URL);
  slack.send({
    text: 'A new version of the Liveshout Admin app has been deployed: <' +
          APP_DEPLOY_URL + '|Open app>'
  }, callback);
}

module.exports = function (done) {
  return sequence('release', 'deploy_firebase', function (err) {
    if (err) return done(err);
    sendNotification(done);
  });
};
