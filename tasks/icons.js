var gulp = require('gulp');
var merge = require('merge-stream');
var replace = require('gulp-replace');
var rename = require('gulp-rename');

module.exports = function () {
  return merge(
    gulp.src('icomoon/style.css')
      .pipe(replace(/fonts\//g, '#{$font-path}/'))
      .pipe(rename('_icons.scss'))
      .pipe(gulp.dest('stylesheets')),
    gulp.src('icomoon/fonts/*.*')
      .pipe(gulp.dest('assets/fonts'))
  );
};
