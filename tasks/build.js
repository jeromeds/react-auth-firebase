var gutil = require('gulp-util');
var sequence = require('run-sequence');
var argv = require('yargs').argv;

module.exports = function (done) {
  gutil.log(
    'BUILD: **%s** is the active environment',
    process.env.NODE_ENV || 'development'
  );
  if (argv.debug) {
    gutil.log('DEBUG mode: LiveReload, source maps and console logging will be enabled');
  }
  if (argv.optimize) {
    gutil.log('OPTIMIZE mode: assets will be minified and optimized');
  }
  return sequence('clean', [
    'scripts',
    'stylesheets',
    'assets',
    'app',
    'localization',
    'config'
  ], done);
};
