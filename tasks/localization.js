var gulp = require('gulp');
var gutil = require('gulp-util');
var merge = require('merge-stream');
var extend = require('gulp-extend');
var argv = require('yargs').argv;
var config = require('../config/config.json');

module.exports = function () {
  return merge(config.availableLocales.map(function (locale) {
    return gulp.src('src/**/locales/' + locale + '.json')
      .pipe(extend(locale + '.json', true, argv.optimize ? undefined : 2))
      .pipe(gulp.dest('public/locales'));
  }));
};
