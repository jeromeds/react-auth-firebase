var gulp = require('gulp');
var spawn = require('child_process').spawn;

module.exports = ['build', function (callback) {
  spawn('firebase', ['deploy'], {
    cwd: './firebase',
    stdio: 'inherit'
  }).on('exit', function (code) {
    if (code) return callback(new Error('exit code: ' + code));
    return callback();
  });
}];
