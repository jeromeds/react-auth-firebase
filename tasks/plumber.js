var plumber = require('gulp-plumber');

module.exports = function () {
  return plumber({
    errorHandler: function (err) {
      console.log('\u0007'); // beep!
      console.log(err.toString());
      this.emit('end');
    }
  });
};
