var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('./plumber');
var cache = require('gulp-cached');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var _autoprefixer = require('gulp-autoprefixer');
var merge = require('merge-stream');
var argv = require('yargs').argv;

var sassOptions = {
  outputStyle: argv.optimize ? 'compressed' : 'expanded'
};

function autoprefixer() {
  return _autoprefixer({
    browsers: 'last 2 versions, > 5%',
    cascade: true
  });
}

function initSourcemaps() {

}

module.exports = function () {
  return merge(
    gulp.src('stylesheets/**/!(_)*.scss')
      .pipe(plumber())
      .pipe(cache('stylesheets'))
      .pipe(argv.debug ? sourcemaps.init() : gutil.noop())
      .pipe(sass(sassOptions))
      .pipe(autoprefixer())
      .pipe(argv.debug ? sourcemaps.write() : gutil.noop())
      .pipe(gulp.dest('public/styles')),
    gulp.src('src/**/!(_)*.scss')
      .pipe(plumber())
      .pipe(cache('app_stylesheets'))
      .pipe(argv.debug ? sourcemaps.init() : gutil.noop())
      .pipe(sass(sassOptions))
      .pipe(autoprefixer())
      .pipe(argv.debug ? sourcemaps.write() : gutil.noop())
      .pipe(gulp.dest('public'))
  );
};
