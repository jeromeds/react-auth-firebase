# liveshout-app

## Setup

### First thing to do after cloning the repo
`bower install`
`npm install`

### Atom users
In order to keep the code clean you need add the fallowing packages:
`linter linter-eslint`

## Git

### Contributing

1. Start working in a new feature branch: `git checkout -b awesomeFeature`
2. Hack, ensuring you stay on top of the latest changes (`git pull && git rebase master`) and commit regularly (`git commit -am "Work on awesome feature"`).
3. When done, push this feature to your fork (`git push origin awesomeFeature`) and create a Pull Request containing your first commits `a,b,c` (through GitHub's web UI). Drag&drop a screenshot of your feature in the PR description.
4. Following PR comments, make changes (commits `d,e,f`) and push them to your fork, which will update the PR.
5. More comments! Make changes, commit them (commits `g,h`) and push them to your fork, which will update the PR.
6. At some point, you may have rebase conflicts with another body of work recently merged. Solve them following the instructions provided by `git rebase master`. If you mess up the conflict resolution, don't panic, you can stop it and restart it safely. When done, `git push --force origin awesomeFeature`
7. When you considers the work ready for merge, you can squash (`a,b,c,d,e,f,g,h` → `s`). If you followed this workflow, your work floats on top of the git tree and you can just `git reset --soft sha1ofstartingpoint` , then re-commit your aggregated work (`git commit -am "Clean detailed summary of your changes."`), and `git push --force origin awesomeFeature` to your fork, which will update the PR.
8. You can now MERGE ;)

### Update

Now if you want to integrate stuff that was merged in master into your fork/feature branch

0. `git pull`
1. `git checkout featurebranch`
2. `git rebase master`

### Lexicon

. Attendee: A person who is present at a specified event;
. Speakers: A person who speaks formally before an audience;
. Note: A brief record of something to assist the memory or for future reference. Can be videos, images, links, quiz, writing, etc;
. Talk: A speech, lecture, conference, etc;
