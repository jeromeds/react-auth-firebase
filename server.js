var express = require('express');
var morgan = require('morgan');
var serveStatic = require('serve-static');
var compression = require('compression');
var parseUrl = require('url').parse;
var app = express();

app.use(morgan('dev'));
app.use(compression());
app.use(serveStatic(__dirname + '/public', { redirect: false }));

app.use(function (req, res) {
  if (!/\.\w+$/.test(parseUrl(req.url).pathname)) {
    // always return index.html for extension-less requests
    res.sendFile(__dirname + '/public/index.html');
  } else {
    res.status(404).end();
  }
});

var port = process.env.PORT || 8081;
var server = app.listen(port, function (err) {
  if (err) throw err;
  console.log('HTTP server started on port %d', port);
});
