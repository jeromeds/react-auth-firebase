// point all resources to the root
System.paths['*'] = '/*';

System.config({
  map: {
    'firebase': 'vendor/firebase.js?v=$VERSION(firebase)',
    'react': 'vendor/react.js?v=$VERSION(react)',
    'react-dom': 'vendor/react-dom.js?v=$VERSION(react-dom)',
    'flux': 'vendor/flux.js?v=$VERSION(flux)',
    'react-router': 'vendor/react-router.js?v=$VERSION(react-router)',
    'history': 'vendor/history.js?v=$VERSION(history)',
    'moment': 'vendor/moment.js?v=$VERSION(moment)',
    'react-datepicker': 'vendor/react-datepicker.js?v=$VERSION(react-datepicker)',
    'tether': 'vendor/tether.js?v=$VERSION(tether)',
    'react-onclickoutside': 'vendor/react-onclickoutside.js?v=$VERSION(react-onclickoutside)',
    'react-dnd': 'vendor/react-dnd.js?v=$VERSION(react-dnd)',
    'react-dnd-html5-backend': 'vendor/react-dnd-html5-backend.js?v=$VERSION(react-dnd-html5-backend)'
  },
  meta: {
    '*.json': {
      // load JSON on-demand
      loader: 'vendor/loaders/json.js'
    },
    '*.css': {
      // load CSS on-demand
      loader: 'vendor/loaders/css.js'
    },
    'browserified.js': {
      // standalone AMD format, produced by Browserify
      format: 'amd'
    }
  }
});

// version-based cache busting
const _locate = System.locate;
System.locate = function (load) {
  var System = this;
  return Promise.resolve(_locate.call(this, load)).then(url => {
    if (!/\?v=/.test(url)) {
      return url + '?v=$VERSION(liveshout-app)';
    }
    return url;
  });
}

// mount the application
System.import('index.js');
