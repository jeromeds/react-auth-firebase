import { dispatch, dispatchAsync } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';
import api from '../api/LoginService.js';
import store from '../stores/ChangePasswordStore.js';

export function updateCredentials(credentials) {
  dispatch(ActionTypes.UPDATE_PASSWORD_CREDENTIALS, { credentials });
}

export function changePassword() {
  dispatch(ActionTypes.CHANGE_PASSWORD);
  if (store.isValid) {
    dispatchAsync(api.changePassword(store.credentials), {
      success: ActionTypes.CHANGE_PASSWORD_SUCCESS,
      failure: ActionTypes.CHANGE_PASSWORD_FAILURE
    });
  }
}

export function resetPassword() {
  dispatch(ActionTypes.RESET_PASSWORD);
  if (store.isValid) {
    dispatchAsync(api.resetPassword(store.credentials), {
      success: ActionTypes.RESET_PASSWORD_SUCCESS,
      failure: ActionTypes.RESET_PASSWORD_FAILURE
    });
  }
}
