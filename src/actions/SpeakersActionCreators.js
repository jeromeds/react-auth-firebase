import { dispatch, dispatchAsync } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';
import db from '../api/FirebaseDB.js';
import store from '../stores/SpeakersStore.js';

export function requestSpeakers() {
  dispatchAsync(db.get(`speakers`), {
    request: ActionTypes.REQUEST_SPEAKERS,
    success: ActionTypes.REQUEST_SPEAKERS_SUCCESS,
    failure: ActionTypes.REQUEST_SPEAKERS_FAILURE
  });
}

export function requestSpeaker(id) {
  dispatchAsync(db.get(`speakers/${id}`), {
    request: ActionTypes.REQUEST_SPEAKER,
    success: ActionTypes.REQUEST_SPEAKER_SUCCESS,
    failure: ActionTypes.REQUEST_SPEAKER_FAILURE
  }, { id });
}

export function update(speaker) {
  dispatch(ActionTypes.UPDATE_SPEAKER, { speaker });
}

export function push(speaker = store.speaker) {
  dispatch(ActionTypes.PUSH_SPEAKER, { speaker });
  dispatchAsync(db.push(`speakers`, speaker), {
    success: ActionTypes.PUSH_SPEAKER_SUCCESS,
    failure: ActionTypes.PUSH_SPEAKER_FAILURE
  }, { speaker });
}

export function save(speaker = store.speaker) {
  dispatch(ActionTypes.SAVE_SPEAKER, { speaker });
  dispatchAsync(db.set(`speakers/${speaker.id}`, speaker), {
    success: ActionTypes.SAVE_SPEAKER_SUCCESS,
    failure: ActionTypes.SAVE_SPEAKER_FAILURE
  });
}

export function remove(id) {
  dispatch(ActionTypes.REMOVE_SPEAKER, { id });
  dispatchAsync(db.remove(`speakers/${id}`), {
    success: ActionTypes.REMOVE_SPEAKER_SUCCESS,
    failure: ActionTypes.REMOVE_SPEAKER_FAILURE
  }, { id });
}

export function reset() {
  dispatch(ActionTypes.RESET_SPEAKER);
}
