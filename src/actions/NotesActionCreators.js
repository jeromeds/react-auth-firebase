import { dispatch, dispatchAsync, dispatchCallback } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';
import db from '../api/FirebaseDB.js';
import NotesStore from '../stores/NotesStore.js';
import TalksStore from '../stores/TalksStore.js';

export function requestNotes() {
  dispatchAsync(db.get(`notes`), {
    request: ActionTypes.REQUEST_NOTES,
    success: ActionTypes.REQUEST_NOTES_SUCCESS,
    failure: ActionTypes.REQUEST_NOTES_FAILURE
  });
}

export function requestTalkNotes(talk = TalksStore.talk) {
  dispatchAsync(db.get(`notes/${talk.id}`), {
    request: ActionTypes.REQUEST_TALK_NOTES,
    success: ActionTypes.REQUEST_TALK_NOTES_SUCCESS,
    failure: ActionTypes.REQUEST_TALK_NOTES_FAILURE
  });
}

export function watch(id) {
  if (id) {
    dispatchCallback(db.watch(`notes/${id}`), {
      request: ActionTypes.REQUEST_NOTE,
      success: ActionTypes.REQUEST_NOTE_SUCCESS,
      failure: ActionTypes.REQUEST_NOTE_FAILURE
    });
  } else {
    dispatch(ActionTypes.REQUEST_NOTE_FAILURE);
  }
}

export function push(note, talk = TalksStore.talk) {
  dispatch(ActionTypes.PUSH_NOTE, { note });
  if (NotesStore.isValid) {
    dispatchAsync(db.push(`notes/${talk.id}`, note), {
      success: ActionTypes.PUSH_NOTE_SUCCESS,
      failure: ActionTypes.PUSH_NOTE_FAILURE
    }, { note });
  }
}

export function update(index, note) {
  dispatch(ActionTypes.UPDATE_NOTE, { index, note });
}

export function save(index, talk = TalksStore.talk) {
  const note = NotesStore.notes[index];
  delete note.isEditable;
  dispatch(ActionTypes.SAVE_NOTE);
  if (NotesStore.isValid) {
    dispatchAsync(db.set(`notes/${talk.id}/${note.id}`, note), {
      success: ActionTypes.SAVE_NOTE_SUCCESS,
      failure: ActionTypes.SAVE_NOTE_FAILURE
    }, { note });
  }
}

export function remove(id, noteIndex, talk = TalksStore.talk) {
  dispatch(ActionTypes.REMOVE_NOTE, { id });
  dispatchAsync(db.remove(`notes/${talk.id}/${id}`), {
    success: ActionTypes.REMOVE_NOTE_SUCCESS,
    failure: ActionTypes.REMOVE_NOTE_FAILURE
  }, { noteIndex });
}

export function edit(noteIndex) {
  dispatch(ActionTypes.EDIT_NOTE, { noteIndex });
}
