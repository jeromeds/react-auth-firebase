import { dispatch, dispatchAsync, dispatchCallback } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';
import db from '../api/FirebaseDB.js';
import store from '../stores/ProfileStore.js';
import * as Tools from '../utils/tools.js';

export function requestProfile() {
  if (!db.auth) {
    dispatch(ActionTypes.REQUEST_PROFILE_FAILURE);
  } else {
    const email = Tools.escapeEmail(db.auth.password.email);
    console.log(email);
    dispatchAsync(db.get(`attendees/${email}`), {
      success: ActionTypes.REQUEST_PROFILE_SUCCESS,
      failure: ActionTypes.REQUEST_PROFILE_FAILURE
    });
  }
}
//
// export function watch() {
//   dispatchCallback(db.watch(`users/${db.auth.uid}`), {
//     request: ActionTypes.REQUEST_PROFILE,
//     success: ActionTypes.REQUEST_PROFILE_SUCCESS,
//     failure: ActionTypes.REQUEST_PROFILE_FAILURE
//   });
// }

export function updateProfile(profile) {
  dispatch(ActionTypes.UPDATE_PROFILE, { profile });
}


// export function save(profile = store.profile) {
//   dispatch(ActionTypes.SAVE_PROFILE);
//   if (store.isValid) {
//     dispatchAsync(db.set(`users/${db.auth.uid}`, profile), {
//       success: ActionTypes.SAVE_PROFILE_SUCCESS,
//       failure: ActionTypes.SAVE_PROFILE_FAILURE
//     }, { profile });
//   }
// }
