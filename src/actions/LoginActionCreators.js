import { dispatch, dispatchAsync } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';
import api from '../api/LoginService.js';
import LoginStore from '../stores/LoginStore.js';
import ProfileStore from '../stores/ProfileStore.js';
import * as Tools from '../utils/tools.js';
import db from '../api/FirebaseDB.js';

export function updateCredentials(credentials) {
  dispatch(ActionTypes.UPDATE_LOGIN_CREDENTIALS, { credentials });
}

export function updateProfile(profile) {
  dispatch(ActionTypes.UPDATE_PROFILE, { profile });
}

export function updateOptions(options) {
  dispatch(ActionTypes.UPDATE_LOGIN_OPTIONS, { options });
}

export function login() {
  dispatch(ActionTypes.LOGIN);
  if (LoginStore.isValid) {
    dispatchAsync(api.login(LoginStore.credentials, LoginStore.options), {
      success: ActionTypes.LOGIN_SUCCESS,
      failure: ActionTypes.LOGIN_FAILURE
    });
  }
};

export function logout() {
  dispatch(ActionTypes.LOGOUT);
  api.logout();
};

export function signup() {
  // Escape email first
  const email = Tools.escapeEmail(LoginStore.credentials.email);
  dispatchAsync(db.get(`attendees/${email}`), {
    request: ActionTypes.VALIDATE_EMAIL,
    success: ActionTypes.VALIDATE_EMAIL_SUCCESS,
    failure: ActionTypes.VALIDATE_EMAIL_FAILURE
  });
}

export function createUser() {
  dispatch(ActionTypes.CREATE_USER);
  //if (LoginStore.isValid) {
    dispatchAsync(api.createUser(LoginStore.credentials, LoginStore.options), {
      success: ActionTypes.CREATE_USER_SUCCESS,
      failure: ActionTypes.CREATE_USER_FAILURE
    });
  //}
}

export function createProfile(profile = ProfileStore.profile) {
  const email = Tools.escapeEmail(LoginStore.credentials.email);
  dispatch(ActionTypes.CREATE_PROFILE, { profile});
  console.log(profile);
  dispatchAsync(db.set(`attendees/${email}`, profile), {
    success: ActionTypes.CREATE_PROFILE_SUCCESS,
    failure: ActionTypes.CREATE_PROFILE_FAILURE
  });
}
