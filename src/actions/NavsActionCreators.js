import { dispatch } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';

export function navigate(infos) {
  dispatch(ActionTypes.NAVIGATE, infos);
}

export function toggleNav(actionType) {
  dispatch(ActionTypes[actionType]);
}
