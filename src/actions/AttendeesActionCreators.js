import { dispatch, dispatchAsync } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';
import db from '../api/FirebaseDB.js';
import store from '../stores/AttendeesStore.js';

export function requestAttendees() {
  dispatchAsync(db.get(`attendees`), {
    request: ActionTypes.REQUEST_ATTENDEES,
    success: ActionTypes.REQUEST_ATTENDEES_SUCCESS,
    failure: ActionTypes.REQUEST_ATTENDEES_FAILURE
  });
}

export function requestAttendee(id) {
  dispatchAsync(db.get(`attendees/${id}`), {
    request: ActionTypes.REQUEST_ATTENDEE,
    success: ActionTypes.REQUEST_ATTENDEE_SUCCESS,
    failure: ActionTypes.REQUEST_ATTENDEE_FAILURE
  }, { id });
}

export function update(attendee) {
  dispatch(ActionTypes.UPDATE_ATTENDEE, { attendee });
}

export function push(attendee = store.attendee) {
  dispatch(ActionTypes.PUSH_ATTENDEE, { attendee });
  dispatchAsync(db.push(`attendees`, attendee), {
    success: ActionTypes.PUSH_ATTENDEE_SUCCESS,
    failure: ActionTypes.PUSH_ATTENDEE_FAILURE
  }, { attendee });
}

export function save(attendee = store.attendee) {
  dispatch(ActionTypes.SAVE_ATTENDEE, { attendee });
  dispatchAsync(db.set(`attendees/${attendee.id}`, attendee), {
    success: ActionTypes.SAVE_ATTENDEE_SUCCESS,
    failure: ActionTypes.SAVE_ATTENDEE_FAILURE
  });
}

export function remove(id) {
  dispatch(ActionTypes.REMOVE_ATTENDEE, { id });
  dispatchAsync(db.remove(`attendees/${id}`), {
    success: ActionTypes.REMOVE_ATTENDEE_SUCCESS,
    failure: ActionTypes.REMOVE_ATTENDEE_FAILURE
  }, { id });
}

export function reset() {
  dispatch(ActionTypes.RESET_ATTENDEE);
}
