import { dispatch, dispatchAsync, dispatchCallback } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';
import db from '../api/FirebaseDB.js';
import NotesStore from '../stores/NotesStore.js';
import ChoicesStore from '../stores/ChoicesStore.js';

export function request(noteId) {
  dispatchAsync(db.get(`choices/${db.auth.uid}/${noteId}`), {
    request: ActionTypes.REQUEST_CHOICES,
    success: ActionTypes.REQUEST_CHOICES_SUCCESS,
    failure: ActionTypes.REQUEST_CHOICES_FAILURE
  });
}

// export function watch(choiceId) {
//   if (choiceId) {
//     dispatchCallback(db.watch(`choices/${db.auth.uid}/${note.id}`), {
//       request: ActionTypes.REQUEST_CHOICE,
//       success: ActionTypes.REQUEST_CHOICE_SUCCESS,
//       failure: ActionTypes.REQUEST_CHOICE_FAILURE
//     });
//   } else {
//     dispatch(ActionTypes.REQUEST_CHOICE_FAILURE);
//   }
// }

export function push(noteId, choices = ChoicesStore.choices) {
  choices.forEach(choice => {
    dispatch(ActionTypes.PUSH_CHOICE, { choice });
    //if (NotesStore.isValid) {
      dispatchAsync(db.push(`choices/${db.auth.uid}/${noteId}`, choice), {
        success: ActionTypes.PUSH_CHOICE_SUCCESS,
        failure: ActionTypes.PUSH_CHOICE_FAILURE
      }, { choice });
    //}
  });
}

export function add(choice) {
  dispatch(ActionTypes.ADD_CHOICE, { choice });
}

export function remove(choiceIndex) {
  dispatch(ActionTypes.DELETE_CHOICE, { choiceIndex });
}

export function update(choice, index) {
  dispatch(ActionTypes.UPDATE_CHOICE, { choice, index });
}

export function save(choice, noteId) {
  dispatch(ActionTypes.SAVE_CHOICE);
  if (NotesStore.isValid) {
    dispatchAsync(db.set(`choices/${db.auth.uid}/${noteId}/${choice.id}`, choice), {
      success: ActionTypes.SAVE_CHOICE_SUCCESS,
      failure: ActionTypes.SAVE_CHOICE_FAILURE
    }, { choice });
  }
}

// export function remove(noteId, choiceId) {
//   dispatch(ActionTypes.REMOVE_CHOICE, { choiceId });
//   dispatchAsync(db.remove(`choices/${db.auth.uid}/${noteId}/${choiceId}`), {
//     success: ActionTypes.REMOVE_CHOICE_SUCCESS,
//     failure: ActionTypes.REMOVE_CHOICE_FAILURE
//   }, { choiceId });
// }

export function edit(choiceIndex) {
  dispatch(ActionTypes.EDIT_CHOICE, { choiceIndex });
}
