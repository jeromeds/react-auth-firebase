import { dispatchAsync } from '../AppDispatcher.js';
import ActionTypes from '../constants/ActionTypes.js';
import db from '../api/FirebaseDB.js';

export function requestTalks() {
  dispatchAsync(db.get(`talks`), {
    request: ActionTypes.REQUEST_TALKS,
    success: ActionTypes.REQUEST_TALKS_SUCCESS,
    failure: ActionTypes.REQUEST_TALKS_FAILURE
  });
}

export function requestTalk(id) {
  dispatchAsync(db.get(`talks/${id}`), {
    request: ActionTypes.REQUEST_TALK,
    success: ActionTypes.REQUEST_TALK_SUCCESS,
    failure: ActionTypes.REQUEST_TALK_FAILURE
  }, { id });
}
