import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import connectToStores from '../../utils/connectToStores.js';
import { requestProfile } from '../../actions/ProfileActionCreators.js';
import ProfileStore from '../../stores/SpeakersStore.js';

function getState() {
  return {
    profile: ProfileStore.profile
  };
}

@connectToStores([ProfileStore], getState)

export default class ProfilePage extends Component {

  static propTypes = {
    profile: PropTypes.object,
  };

  componentWillMount() {
    requestProfile();
  }

  render() {
    return (
      <section className="layout">
        [profile]
      </section>
    );
  }
}
