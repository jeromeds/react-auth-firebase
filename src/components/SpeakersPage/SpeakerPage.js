import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { Router, Route, IndexRoute } from 'react-router';
import connectToStores from '../../utils/connectToStores.js';
import { requestSpeaker } from '../../actions/SpeakersActionCreators.js';
import { requestTalks } from '../../actions/TalksActionCreators.js';
import SpeakersStore from '../../stores/SpeakersStore.js';
import TalksStore from '../../stores/TalksStore.js';
import './speakers.css';
import Icon from '../../utils/icon.js';
import moment from 'moment';

function getState() {
  return {
    speaker: SpeakersStore.speaker,
    talks: TalksStore.talks
  };
}

@connectToStores([SpeakersStore, TalksStore], getState)

export default class SpeakerPage extends Component {

  static propTypes = {
    params: PropTypes.object,
    speaker: PropTypes.object,
    talks: PropTypes.array
  };

  componentWillMount() {
    requestSpeaker(this.props.params.id);
    requestTalks();
  }

  _showSocialIcon(type, link){
    if (link === undefined) {
      return null;
    }
    return (
      <a to={link} target='_blank'>
        <Icon icon={`${type}`} />
      </a>
    );
  }

  _showSocialMedia(){
    const { speaker } = this.props;

    if (!speaker.hasOwnProperty('twitter') && !speaker.hasOwnProperty('facebook') && !speaker.hasOwnProperty('linkedin')) { //todo: improve this
      return null;
    }

    return (
      <div className="block socials">
        <div className="bg">
          {this._showSocialIcon('twitter', speaker.twitter)}
          {this._showSocialIcon('facebook', speaker.facebook)}
          {this._showSocialIcon('linkedin', speaker.linkedin)}
        </div>
      </div>
    );
  }

  _getTalks() {
    const { params, talks } = this.props;
    return talks.filter(x => x.hasOwnProperty('speakers') && x.speakers.indexOf(params.id) >=0);
  }

  _showTalks() {
    const talks = this._getTalks();

    if (!talks.length) {
      return;
    }

    return (
      <div className="block talks">
        <div className="sectionTitle">Talks</div>
        <div className="bg">
          {talks.map(x => {
            return (
              <Link className="row list" key={`items_${x.id}`}  to={`/talk/${x.id}`}>
                <div className="col col-1"><div className="avatar-ctn"><div className="avatar"></div></div></div>
                <div className="col col-2 infos item-2">
                  <div className="time">{moment.unix(x.starting).format('MMMM DD')} - {moment.unix(x.starting).format('HH:mm')} to {moment.unix(x.ending).format('HH:mm')}</div>
                  <div className="name">{x.name}</div>

                </div>
                <div className="col col-3 icon">
                  <Icon icon="arrowRight" />
                </div>
              </Link>
            );
          })}
        </div>
      </div>
    );
  }

  _showBio() {
    const { speaker } = this.props;

    if(!speaker.hasOwnProperty('bio')){
      return null;
    }

    return (
      <div className="block bio">
        <div className="sectionTitle">Bio</div>
        <div className="bg spacer">
          <p>
            {speaker.bio}
          </p>
        </div>
      </div>
    );
  }

  render() {
    const { speaker } = this.props;

    return (
      <section className="layout">
        <header>
          <div className="infos">
            <div className="avatar-ctn">
             <img className="avatar avatar-large" src={speaker.avatar} />
            </div>
            <div className="name">{speaker.firstName} {speaker.lastName}</div>
            <div className="title">{speaker.company}, {speaker.title}</div>
          </div>
        </header>
        <div className="body">
          {this._showSocialMedia()}
          {this._showTalks()}
          {this._showBio()}
        </div>
      </section>
    );
  }
}
