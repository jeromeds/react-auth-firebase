import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import connectToStores from '../../utils/connectToStores.js';
import { requestSpeakers } from '../../actions/SpeakersActionCreators.js';
import SpeakersStore from '../../stores/SpeakersStore.js';
import './speakers.css';
import Icon from '../../utils/icon.js';

function getState() {
  return {
    items: SpeakersStore.speakers
  };
}

@connectToStores([SpeakersStore], getState)

export default class SpeakersPage extends Component {

  static propTypes = {
    items: PropTypes.array
  };

  componentWillMount() {
    requestSpeakers();
  }

  render() {
    const { items } = this.props;
    return (
      <section className="layout">
        <div className="body">
          <div className="block">
            <div className="bg">
              {items.map(item => {
                return (
                  <Link className="row list" key={`items_${item.id}`}  to={`/speaker/${item.id}`}>
                    <div className="col col-1"><div className="avatar-ctn"><img className="avatar avatar-small" src={item.avatar} /></div></div>
                    <div className="col col-2 infos item-2">
                      <div className="name">{item.firstName} {item.lastName}</div>
                      <div className="title">{item.company}, {item.title}</div>
                    </div>
                    <div className="col col-3 icon">
                      <Icon icon="arrowRight" />
                    </div>
                  </Link>
                );
              })}
            </div>
          </div>
        </div>
      </section>
    );
  }
}
