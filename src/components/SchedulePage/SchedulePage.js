import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import connectToStores from '../../utils/connectToStores.js';
import { requestSpeakers } from '../../actions/SpeakersActionCreators.js';
import { requestTalks } from '../../actions/TalksActionCreators.js';
import SpeakersStore from '../../stores/SpeakersStore.js';
import TalksStore from '../../stores/TalksStore.js';
import moment from 'moment';
import Icon from '../../utils/icon.js';

function getState() {
  return {
    speakers: SpeakersStore.speakers,
    talks: TalksStore.talks
  };
}

@connectToStores([TalksStore], getState)

export default class SchedulePage extends Component {

  static propTypes = {
    talks: PropTypes.array,
    speakers: PropTypes.array,
  };

  componentWillMount() {
    requestSpeakers();
    requestTalks();
  }

  _getSpeaker(id) {
    const { speakers } = this.props;
    return speakers.filter( x => {
      return x.id === id;
    });
  }

  _getSpeakers(talk) {
    if (!talk.hasOwnProperty('speakers') || !talk.speakers.length) {
      return null;
    }

    return talk.speakers.map(x => {
      const speaker = this._getSpeaker(x)[0]; //todo find other solutiion for [0]
      return speaker.hasOwnProperty('firstName') ? speaker.firstName : '';
    }).join(', ');
  }

  _showSpeaker(talk) {
    const speaker = this._getSpeakers(talk);
    if (!speaker) {
      return null;
    }
    return (
      <div className="speaker">By {this._getSpeakers(talk)}</div>
    )
  }

  _getTalks(time) {
    const { talks } = this.props;
    return (
      <div className="bg">
        <div className="row header">
          <div className="col col-1"></div>
          <div className="col col-2 text-right">{moment.unix(time).format('DD MMMM')}</div>
        </div>
      {
        talks.filter(talk => talk.starting === time)
        .map(talk => {
          return (
            <Link className="row list" key={`item_${talk.id}`}  to={`/talk/${talk.id}`}>
              <div className="col col-1"><div className="avatar-ctn"><div className="avatar"></div></div></div>
              <div className="col col-2 infos">
                <div className="time">{moment.unix(talk.starting).format('HH:mm')} to {moment.unix(talk.ending).format('HH:mm')}</div>
                <div className="name">{talk.name}</div>
                {this._showSpeaker(talk)}
              </div>
              <div className="col col-3 icon">
                <Icon icon="arrowRight" />
              </div>
            </Link>
          )
        })
      }
      </div>
    );
  }

  _getTimeline() {
    const { talks } = this.props;
    let timeline = new Set();

    if (!talks.length) {
      return null;
    }

    talks.filter(x => moment.unix(x.starting).isValid())
      .sort(x => x.starting)
      .forEach(talk => {
        //set.add(moment.unix(talk.starting).format('YYYY-MM-DD'));
        timeline.add(talk.starting);
      });

    timeline = Array.from(timeline);
    return (
      <div className="body">
      {timeline.map(x => {
        return (
          <div className="block">
            {this._getTalks(x)}
          </div>
        );
      })};
      </div>
    );
  }

  render() {
    return (
      <section className="layout">
        {this._getTimeline()}
      </section>
    );
  }
}
