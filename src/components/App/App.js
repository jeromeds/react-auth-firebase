import React, { Component, PropTypes } from 'react';
import TopNav from '../navs/TopNav.js';
import LeftNav from '../navs/LeftNav.js';
import RightNav from '../navs/RightNav.js';
import { navigate } from '../../actions/NavsActionCreators.js';

export default class App extends Component {
  static propTypes = {
    children: PropTypes.object
  };

  componentDidMount() {
    this._updateNavsStore();
  }

  componentDidUpdate() {
    this._updateNavsStore();
  }

  _updateNavsStore(){
    navigate({ name: this._getCurrentPageName() });
  }

  _getCurrentPageName() {
    const { routes } = this.props;
    console.log(routes[routes.length -1].name);
    return routes[routes.length -1].name;
  }

  render() {
    return (
      <div className={`container ${this._getCurrentPageName()}`}>
        <TopNav />
        <LeftNav />
        <RightNav />
        <div className="content">
          {this.props.children}
        </div>
      </div>
    );
  }
}
