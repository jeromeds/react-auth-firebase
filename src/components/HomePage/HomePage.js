import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import AttendeesStore from '../../stores/AttendeesStore.js';
import TalksStore from '../../stores/TalksStore.js';
import SpeakersStore from '../../stores/SpeakersStore.js';
import { requestAttendees} from '../../actions/AttendeesActionCreators.js';
import { requestTalks } from '../../actions/TalksActionCreators.js';
import { requestSpeakers } from '../../actions/SpeakersActionCreators.js';
import connectToStores from '../../utils/connectToStores.js';
import './home.css';

function getState() {
  return {
    attendees: AttendeesStore.attendees,
    speakers: SpeakersStore.speakers,
    talks: TalksStore.talks
  };
}

@connectToStores([TalksStore, AttendeesStore, SpeakersStore], getState)

export default class HomePage extends Component {

  static propTypes = {
    attendees: PropTypes.array,
    speakers: PropTypes.array,
    talks: PropTypes.array
  };

  componentWillMount() {
    requestAttendees();
    requestSpeakers();
    requestTalks();
  }

  render() {
    const { attendees, speakers, talks } = this.props;
    return (
        <section className="layout">
          <div className="body">
          <div className="block home">
            <div className="bg row">
            <Link className="col-2" to={`/schedule`}>
              <div className="title"><span className="number">{talks.length}</span> Talks</div>
            </Link>
            <Link to={`/speakers`}>
              <div className="title"><span className="number">{speakers.length}</span> Speakers</div>
            </Link>
            <Link to={`/messages`}>
              <div className="title">Messages</div>
            </Link>
            <Link to={`/notes`}>
              <div className="title">Notes</div>
            </Link>
          </div>
        </div>
      </div>
      </section>
    );
  }
}
