import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import './IntroPage.css';

import requiresAuthentication from '../../utils/requiresAuthentication.js';
//@requiresAuthentication()

export default class HomePage extends Component {
  static propTypes = {
  };
  render() {
    return (
      <section className="layout intro">
        <div className="box">
          <h1>Sessions Fanslab</h1>
          <p>Créer des événements engageants: Stratégie, Technologie & mesure comme gage de succès</p>
          <Link className="btn btn-1" to="/login">
            Sign In
          </Link>
          <Link className="btn btn-2" to="/home">
            Guest
          </Link>
        </div>
        <div className="bottom-link">
        </div>
      </section>
    );
  }
}
