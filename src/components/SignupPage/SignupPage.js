import React, { Component, PropTypes } from 'react';
import { updateCredentials, signup } from '../../actions/LoginActionCreators.js';
import { updateProfile } from '../../actions/ProfileActionCreators.js';
import LoginStore from '../../stores/LoginStore.js';
import ProfileStore from '../../stores/ProfileStore.js';
//import SignupStore from '../../stores/SignupStore.js';
import connectToStores from '../../utils/connectToStores.js';
import './SignupPage.css';

function getState() {
  return {
    credentials: LoginStore.credentials,
    error: LoginStore.error,
    profile: ProfileStore.profile
    //user: SignupStore.user,
    //isEmailExist: SignupStore.isEmailExist,
    //isFormValid: SignupStore.isFormValid,
    //isUserActive: SignupStore.isUserActive
  };
}

@connectToStores([LoginStore, ProfileStore], getState)

export default class SignupPage extends Component {

  componentDidUpdate() {
    //const { isEmailExist, isFormValid, isUserActive} = this.props;
    // To register you need 2 things:
    // - email already in DB
    // - email is not activated
    // console.log(isEmailExist, isFormValid, isUserActive);
    // if (isEmailExist && isFormValid && !isUserActive) {
    //   console.log('submit form');
    //   setTimeout(function () {
    //       signup();
    //   }, 1);
    // }
  }

  _handleSubmit(e) {
    e.preventDefault();
    //let { credentials, options } = loginStore;
    //console.log(this);
    //validateEmail();
    signup();
    //signup(credentials, options);
  }

  _showError() {
    const { error } = this.props;
    if (!error) {
      return null;
    }
    return error.message;
  }

  render() {
    const { credentials, profile } = this.props;
    return (
      <section className="layout">
        <form onSubmit={this._handleSubmit}>
          <div className="bg">
            <div className="row">
                <label htmlFor="inputUsername" className="form-label col col-1">
                  Username
                </label>
                <input
                  ref="username"
                  type="text"
                  id="inputUsername"
                  className="form-input  col col-2"
                  placeholder="Username example"
                  autoFocus
                  onChange={e => updateProfile({ username: e.target.value })}
                  value={profile.username}
                />
            </div>
            <div className="row">
              <label htmlFor="inputEmail" className="form-label col col-1">
                Email
              </label>
              <input
                ref="email"
                type="text"
                id="inputPassword"
                className="form-input col col-2"
                placeholder="Email example"
                onChange={e => updateCredentials({ email: e.target.value })}
                value={credentials.email}
              />
            </div>
            <div className="row">
              <label htmlFor="inputPassword" className="form-label col col-1">
                Password
              </label>
              <input
                ref="password"
                type="password"
                id="inputPassword"
                className="form-input col col-2"
                onChange={e => updateCredentials({ password: e.target.value })}
                value={credentials.password}
              />
            </div>
            <div className="errors">{this._showError()}</div>
          </div>
          <button
            type="submit"
            className="btn btn-2"
          >
            Create Account
          </button>
        </form>
        <div className="bottom-link">
        </div>
      </section>
    );
  }
}
