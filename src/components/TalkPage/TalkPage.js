import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import connectToStores from '../../utils/connectToStores.js';
import { requestSpeakers } from '../../actions/SpeakersActionCreators.js';
import { requestTalk } from '../../actions/TalksActionCreators.js';
import SpeakersStore from '../../stores/SpeakersStore.js';
import TalksStore from '../../stores/TalksStore.js';
import './talk.css';
import Icon from '../../utils/icon.js';

function getState() {
  return {
    speakers: SpeakersStore.speakers,
    talk: TalksStore.talk
  };
}

@connectToStores([SpeakersStore, TalksStore], getState)

export default class TalkPage extends Component {

  static propTypes = {
    params: PropTypes.object,
    speakers: PropTypes.array,
    talk: PropTypes.object
  };

  componentWillMount() {
    requestSpeakers();
    requestTalk(this.props.params.id);
  }

  _getSpeaker(id) {
    const { speakers } = this.props;
    return speakers.filter( x => {
      return x.id === id;
    });
  }

  _showSpeakers() {
    const { talk } = this.props;

    if (!talk.hasOwnProperty('speakers') || !talk.speakers.length) {
      return;
    }
    return (
      <div className="block">
        <div className="sectionTitle">Speakers</div>
        <div className="bg">
          {talk.speakers.map(x => {
            const speaker = this._getSpeaker(x)[0]; //todo find other solutiion for [0]
            //return <Link className="btn btn-secondary" to={`/speaker/${x}`}>{speaker.firstName}</Link>;
            return(
              <Link className="row list" key={`items_${x}`}  to={`/speaker/${x}`}>
                <div className="col col-1"><div className="avatar-ctn"><div className="avatar"></div></div></div>
                <div className="col col-2 infos item-2">
                  <div className="name">{speaker.firstName} {speaker.lastName}</div>
                  <div className="title">{speaker.title}, {speaker.company}</div>
                </div>
                <div className="col col-3 icon">
                  <Icon icon="arrowRight" />
                </div>
              </Link>
            );
          })}
        </div>
      </div>
    );
  }

  render() {
    const { talk } = this.props;

    return (
      <section className="layout">
        <header>
          <div className="infos">
            <div className="name">{talk.name}</div>
            <div className="title">{talk.starting} - {talk.ending}</div>
          </div>
        </header>
        <div className="body">
          {this._showSpeakers()}
        </div>
      </section>
    );
  }
}
