import React, { Component, PropTypes } from 'react';
import connectToStores from '../../utils/connectToStores.js';
import { requestAttendees } from '../../actions/AttendeesActionCreators.js';
import AttendeesStore from '../../stores/AttendeesStore.js';

function getState() {
  return {
    items: AttendeesStore.attendees
  };
}

@connectToStores([AttendeesStore], getState)

export default class AttendeesPage extends Component {

  static propTypes = {
    items: PropTypes.array
  };

  componentWillMount() {
    requestAttendees();
  }

  _getItemsRow(params) {
    return (
      <div key={`items_${params.id}`}>
        {params.firstName} {params.lastName}
      </div>
    );
  }

  _getItems() {
    const { items } = this.props;

    if (!items.length) {
      return null;
    }

    let itemRow = [];
    items.forEach(item => {
      itemRow.push(this._getItemsRow(item));
    });
    return itemRow;
  }

  render() {
    return (
      <div>
        {this._getItems()}
      </div>
    );
  }
}
