import React, { Component, PropTypes } from 'react';
import ActionTypes from '../../constants/ActionTypes.js';
import { Link } from 'react-router';
import PageConstants from '../../constants/PageConstants.js';
import * as AppConfig from '../../constants/AppConfig.js';
import NavsStore from '../../stores/NavsStore.js';
import connectToStores from '../../utils/connectToStores.js';
import { toggleNav } from '../../actions/NavsActionCreators.js';
import Icon from '../../utils/icon.js';
import './navs.css';

function getState() {
  return {
    currentPage: NavsStore.currentPage,
    isRightNav: NavsStore.isRightNav
  };
}

@connectToStores([NavsStore], getState)

export default class RightNav extends Component {

  static propTypes = {
    currentPage: PropTypes.object,
    isRightNav: PropTypes.bool
  };

  _handleClick() {
    toggleNav(ActionTypes.TOGGLE_RIGHT_NAV);
  }

  render() {
    const { isRightNav, currentPage } = this.props;

    return (
      <div className="rightNav">
        <div className={`bg${isRightNav ? ' active' : ''}`} onClick={this._handleClick} />
        <div className={`list${isRightNav ? ' active' : ''}`}>
          <ul>
              <li>
                <a>
                  {currentPage.name}
                </a>
              </li>
          </ul>
        </div>
      </div>
    );
  }
}
