import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { toggleNav } from '../../actions/NavsActionCreators.js';
import NavsStore from '../../stores/NavsStore.js';
import connectToStores from '../../utils/connectToStores.js';
import ActionTypes from '../../constants/ActionTypes.js';
import PageConstants from '../../constants/PageConstants.js';
import * as Tools from '../../utils/tools.js';
import Icon from '../../utils/icon.js';
import './navs.css';

function getState() {
  return {
    currentPage: NavsStore.currentPage,
    showRightNav: NavsStore.showRightNav
  };
}

@connectToStores([NavsStore], getState)

export default class TopNav extends Component {
  static propTypes = {
    currentPage: PropTypes.object,
    showRightNav: PropTypes.bool
  };

  _toggleLeftNav(e) {
    e.preventDefault();
    toggleNav(ActionTypes.TOGGLE_LEFT_NAV);
  }

  _toggleRightNav(e) {
    e.preventDefault();
    toggleNav(ActionTypes.TOGGLE_RIGHT_NAV);
  }

  _getLeftButton() {
    const { currentPage } = this.props;

    if (!Tools.isLeftNav(currentPage.name)) {
      return null;
    }

    if (!PageConstants.pageParent.hasOwnProperty(currentPage.name)) {
      return (
        <button className="btn top-left menu" onClick={this._toggleLeftNav}>
          <Icon icon="menu" />
        </button>
      );
    }
    return (
      <Link className="btn top-left back" to={`/${PageConstants.pageParent[currentPage.name]}`}>
        <Icon icon="arrowLeft" />
      </Link>
    );
  }

  _getRightButton() {
    const { showRightNav, currentPage } = this.props;

    if (!Tools.isRightNav(currentPage.name)) {
      return null;
    }

    if (currentPage.name === PageConstants.pageNames.MESSAGES) {
      return (
        <button className="btn top-right" onClick={this._toggleRightNav}>
          <Icon icon="messages" />
        </button>
      );
    }else if (currentPage.name === PageConstants.pageNames.NOTES) {
      return (
        <button className="btn top-right"  onClick={this._toggleRightNav}>
          <Icon icon="note" />
        </button>
      );
    }
  }

  _getTitle() {
    const { currentPage } = this.props;
    let name = currentPage.name;
    if (name) {
      name = name.charAt(0).toUpperCase() + name.slice(1);
    }
    return <div className="title">{name}</div>;
  }

  render() {
    const { currentPage } = this.props;

    if (!Tools.isTopNav(currentPage.name)) {
      return null;
    }

    return (
      <div className="topNav">
        {this._getLeftButton()}
        {this._getTitle()}
        {this._getRightButton()}
      </div>
    );
  }
}
