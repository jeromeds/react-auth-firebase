import React, { Component, PropTypes } from 'react';
import ActionTypes from '../../constants/ActionTypes.js';
import PageConstants from '../../constants/PageConstants.js';
import * as AppConfig  from '../../constants/AppConfig.js';
import { Link } from 'react-router';
import NavsStore from '../../stores/NavsStore.js';
import connectToStores from '../../utils/connectToStores.js';
import AuthStore from '../../stores/AuthStore.js';
import ProfileStore from '../../stores/ProfileStore.js';
import { toggleNav } from '../../actions/NavsActionCreators.js';
import { requestProfile } from '../../actions/ProfileActionCreators.js';
import { logout } from '../../actions/LoginActionCreators.js';
import Icon from '../../utils/icon.js';
import './Navs.css';

function getState() {
  return {
    authenticated: AuthStore.authenticated,
    currentPage: NavsStore.currentPage,
    isLeftNav: NavsStore.isLeftNav,
    profile: ProfileStore.profile
  };
}

@connectToStores([NavsStore, AuthStore, ProfileStore], getState)

export default class LeftNav extends Component {

  static propTypes = {
    authenticated: PropTypes.bool,
    currentPage: PropTypes.object,
    isLeftNav: PropTypes.bool,
    profile: PropTypes.object
  };

  componentWillMount() {
    requestProfile();
  }

  _handleLogoutClick(e) {
    e.preventDefault();
    logout();
  }

  _handleClick() {
    toggleNav(ActionTypes.TOGGLE_LEFT_NAV);
  }

  _getMenuItems() {
    return [
      PageConstants.pageNames.HOME,
      PageConstants.pageNames.SCHEDULE,
      PageConstants.pageNames.SPEAKERS,
      PageConstants.pageNames.MESSAGES,
      PageConstants.pageNames.NOTES
    ]
  }

  _getIconName(name) {
    return AppConfig.pageParams[name].icon;
  }

  _showIcon(name) {
    if (this._getIconName(name) === undefined) {
      return null;
    }
    return <Icon icon={this._getIconName(name)} />;
  }

  _getAuthButton() {
    const { authenticated } = this.props;

    if (authenticated) {
      return (
        <li>
          <Link
            className="btn btn-secondary"
            to={`/logout`}
            onClick={this._handleLogoutClick}
          >
            <Icon icon="logout" /> Logout
          </Link>
        </li>
      );
    }

    return (
      <li>
        <Link
          className="btn btn-secondary"
          to={`/login`}
          onClick={this._handleClick}
        >
          Login
        </Link>
      </li>
    );
  }

  _showProfile() {
    const { profile } = this.props;
    if (!profile) {
      return null;
    }
    return (
      <div className="infos-ctn">
        <div className="avatar-ctn"><img src={profile.avatar} className="avatar avatar-small" /></div>
        <div className="name">{profile.firstName} {profile.lastName}</div>
      </div>
    );
  }

  render() {
    const { currentPage, isLeftNav } = this.props;

    return (
      <div className="leftNav">
        <div className={`bg${isLeftNav ? ' active' : ''}`} onClick={this._handleClick} />
        <div className={`list${isLeftNav ? ' active' : ''}`}>
          <Link className="top" to="/profile">{this._showProfile()}</Link>
          <ul>
          {this._getMenuItems().map(x => {
            return (
              <li key={x}>
                <Link
                  className={`btn btn-secondary${currentPage.group === x ? ' active' : ''}`}
                  to={`/${x}`}
                  name={x}
                  onClick={this._handleClick}
                >
                  <div className="icon-ctn">{this._showIcon(x)}</div> {x.charAt(0).toUpperCase() + x.slice(1)}
                </Link>
              </li>
            );
          })}
          {this._getAuthButton()}
          </ul>
        </div>
      </div>
    );
  }
}
