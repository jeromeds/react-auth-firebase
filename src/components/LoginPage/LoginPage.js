import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { updateCredentials, updateOptions, login } from '../../actions/LoginActionCreators.js';
import LoginStore from '../../stores/LoginStore.js';
import connectToStores from '../../utils/connectToStores.js';
import localized from '../../utils/localized.js';
import './LoginPage.css';

function getState() {
  return {
    credentials: LoginStore.credentials,
    error: LoginStore.error,
    isLoggingIn: LoginStore.isLoggingIn,
    options: LoginStore.options,
    validationErrors: LoginStore.validationErrors || []
  };
}

@connectToStores([LoginStore], getState)
@localized('login')

export default class LoginPage extends Component {

  static propTypes = {
    credentials: PropTypes.shape({
      email: PropTypes.string,
      password: PropTypes.string
    }),
    error: PropTypes.shape({
      code: PropTypes.string
    }),
    isLoggingIn: PropTypes.bool,
    options: PropTypes.shape({
      remember: PropTypes.bool
    }),
    validationErrors: PropTypes.arrayOf(PropTypes.string)
  };

  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(e) {
    e.preventDefault();
    login();
  }

  _showError() {
    const { error } = this.props;
    if (!error) {
      return null;
    }
    return error.message;
  }

  render() {
    const {
      credentials,
      options,
      isLoggingIn
    } = this.props;

    return (
      <section className="layout login">

        <form onSubmit={this.handleSubmit}>
          <div className="bg">
            <div className="row">
                <label htmlFor="inputEmail" className="form-label col col-1">
                  {this.t('emailAddress')}
                </label>
                <input
                  ref="email"
                  type="email"
                  id="inputEmail"
                  className="form-input  col col-2"
                  placeholder={this.t('emailAddress')}

                  autoFocus
                  value={credentials.email}
                  onChange={e => updateCredentials({ email: e.target.value })}
                />
            </div>
            <div className="row">
              <label htmlFor="inputPassword" className="form-label  col col-1">
                {this.t('password')}
              </label>
              <input
                ref="password"
                type="password"
                id="inputPassword"
                className="form-input col col-2"
                placeholder={this.t('password')}

                value={credentials.password}
                onChange={e => updateCredentials({ password: e.target.value })}
              />
            </div>
            <div className="row">
              <div className="col col-2 float-right checkbox">
                <label>
                  <input
                    type="checkbox"
                    value="remember-me"
                    checked={options.remember}
                    onChange={e => updateOptions({ remember: e.target.checked })}
                  />
                {this.t('rememberMe')}
                </label>
              </div>
            </div>
            <div className="errors">{this._showError()}</div>
          </div>
          <button
            type="submit"
            disabled={isLoggingIn}
            className="btn btn-1"
          >
            {this.t(isLoggingIn ? 'signingIn' : 'signIn')}
          </button>
        </form>
        <div className="bottom-link">
          Not a member? &nbsp;
          <Link to="/signup">
            Sign up now!
          </Link>
        </div>
      </section>
    );
  }
}
