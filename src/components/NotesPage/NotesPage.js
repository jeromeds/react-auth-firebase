import React, { Component, PropTypes } from 'react';
import connectToStores from '../../utils/connectToStores.js';
import { requestNotes } from '../../actions/NotesActionCreators.js';
import NotesStore from '../../stores/NotesStore.js';

function getState() {
  return {
    notes: NotesStore.notes
  };
}

@connectToStores([NotesStore], getState)

export default class NotesPage extends Component {

  static propTypes = {
    notes: PropTypes.object,
    params: PropTypes.object
  };

  componentWillMount() {
    requestNotes();
  }

  _getNotes() {
    const { notes, params } = this.props;
    if (!notes) {
      return [];
    }
    let talkNotes = Object.keys(notes[params.id]).map(key => {
      notes[params.id][key].id = key;
      return notes[params.id][key];
    });
    return talkNotes;
  }

  render() {
    return (
      <div>
        Notes
        <div>
        {this._getNotes().map(note => {
          return (
            <div key={note.id}>
              <div>{note.id}</div>
              <div>{note.type}</div>
              <div>{note.dateTime}</div>
            </div>
          );
        })}
        </div>
      </div>
    );
  }
}
