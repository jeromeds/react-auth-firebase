import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import connectToStores from '../../utils/connectToStores.js';
import { requestNotes } from '../../actions/NotesActionCreators.js';
import { requestTalks } from '../../actions/TalksActionCreators.js';
import NotesStore from '../../stores/NotesStore.js';
import TalksStore from '../../stores/TalksStore.js';
import Icon from '../../utils/icon.js';

function getState() {
  return {
    notes: NotesStore.notes,
    talks: TalksStore.talks
  };
}

@connectToStores([NotesStore, TalksStore], getState)

export default class NotesHomePage extends Component {

  static propTypes = {
    notes: PropTypes.object,
    talks: PropTypes.array
  };

  componentWillMount() {
    requestNotes();
    requestTalks();
  }

  _getNotes(talkId) {
    let notes = this.props.notes[talkId];
    return notes;
  }

  _getNotesCount(talkId) {
    const { notes } = this.props;
    if (!notes.hasOwnProperty(talkId)) {
      return 0;
    }
    return `(${Object.keys(notes[talkId]).length})`;
  }

  _getTalksWithNotes() {
    const { notes, talks } = this.props;
    if (!notes) {
      return [];
    }
    return talks.filter(talk => notes.hasOwnProperty(talk.id));
  }

  render() {
    return (
      <div className="layout">
        {this._getTalksWithNotes().map(talk => {
          return (
            <div className="body" key={talk.id}>
                <div className="block">
                  <div className="bg">
                    <Link className="row list" to={`/note/${talk.id}`}>
                      <div className="col col-1"><div className="avatar-ctn"><div className="avatar"></div></div></div>
                      <div className="col col-2 infos">
                        <div className="name">{talk.name}</div>
                        <div>{this._getNotesCount(talk.id)}</div>
                      </div>
                      <div className="col col-3 icon">
                        <Icon icon="arrowRight" />
                      </div>
                    </Link>
                  </div>
                </div>
            </div>
          );
        })}
      </div>
    );
  }
}
