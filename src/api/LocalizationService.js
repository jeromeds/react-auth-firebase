import { defaultLocale, availableLocales } from '../config.json';
import { sprintf } from '../browserified.js';

let initialized;

/**
 * The localization API
 */
export default class LocalizationService {

  /**
   * Loads localization data from a JSON file
   * The default files are loaded from "./locales"
   * @param  {string} [path] The path of the file to load, if different from the default
   * @return {Promise}       A promise to be fulfilled when the file is loaded
   */
  static load(path) {
    let locale = LocalizationService.negotiate();
    if (!path) path = `/locales/${locale}.json`;
    return System.import(path);
  }

  /**
   * Retrieves a localization property located at a specific path
   * @param  {Object} phrases   The localization data object
   * @param  {string} key       The path of the localization data to retrieve
   * @param  {string[]} format  printf-style formatting arguments
   * @return {string|Object}    The localized property
   */
  static translate(phrases, key, ...format) {
    if (!key) return phrases;
    let current = phrases;
    for (let path of key.split('.')) {
      if (typeof current === 'object' && path in current) {
        current = current[path];
      } else {
        let locale = LocalizationService.negotiate();
        console.warn(`Failed to localize "${key}" for locale "${locale}"`);
        return key;
      }
    }
    if (format.length && typeof current === 'string') {
      return sprintf(current, ...format);
    }
    return current;
  }

  /**
   * Negotiates a locale according to configured available locales
   * @param  {string} [locale] The locale string (ex: "en-US")
   * @return {string}          The resulting locale, representing the best match against available locales
   */
  static negotiate(locale) {
    if (!locale) locale = LocalizationService.getLocale();
    if (availableLocales.indexOf(locale) !== -1) return locale;
    locale = locale.substr(0, 2);
    if (availableLocales.indexOf(locale) !== -1) return locale;
    return defaultLocale;
  }

  /**
   * Returns the currently selected locale or the browser default
   * @return {string} The selected locale string
   */
  static getLocale() {
    let preferredLocale = localStorage.getItem('preferredLocale');
    let browserLocale = navigator.languages ?
      navigator.languages[0] :
      (navigator.language || navigator.userLanguage);
    return preferredLocale || browserLocale;
  }

  /**
   * Assigns a selected locale, overriding the browser default
   * @param {string} locale The locale string
   */
  static setLocale(locale) {
    localStorage.setItem('preferredLocale', locale);
    initialized && LocalizationService.initialize();
  }

}
