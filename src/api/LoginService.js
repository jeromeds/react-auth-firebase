import Firebase from 'firebase';
import config from '../config.json';
import { EventEmitter } from '../browserified.js';

const { passwordPolicy } = config;

/**
 * The authentication and password management API
 */
export class LoginService extends EventEmitter {

  constructor(ref) {
    super();
    if (!ref) throw new Error('Firebase reference is required');
    this.ref = ref;
    this.ref.onAuth(auth => {
      this.emit('login', auth);
    });
    this.ref.offAuth(() => {
      this.emit('logout');
    });
  }

  /**
   * Queries the current authentication state
   * @return {Object} The authentication details, or null if the user is not authenticated
   */
  current() {
    return this.ref.getAuth();
  }

  /**
   * Performs a login attempt
   * @param  {Object} credentials The user credentials
   * @param  {Object} options     The authentication options
   * @return {Promise}            A promise to be fulfilled when authentication is completed
   */
  login(credentials, options) {
    return new Promise((resolve, reject) => {
      this.ref.authWithPassword(credentials, (err, auth) => {
        if (err) return reject(err);
        resolve(auth);
      }, options);
    });
  }

  /**
   * Logout attempt, disconnecting the current user
   */
  logout() {
    this.ref.unauth();
  }

  /**
   * Changes the password for a user
   * @param  {Object} credentials The user credentials
   * @return {Promise}            A promise to be fulfilled when the request is completed
   */
  changePassword(credentials) {
    return new Promise((resolve, reject) => {
      this.ref.changePassword(credentials, err => {
        err ? reject(err) : resolve();
      });
    });
  }

  /**
   * Resets the password for a user
   * @param  {Object} credentials The user credentials
   * @return {Promise}            A promise to be fulfilled when the request is completed
   */
  resetPassword(credentials) {
    return new Promise((resolve, reject) => {
      this.ref.resetPassword(credentials, err => {
        err ? reject(err) : resolve();
      });
    });
  }

  /**
   * Create new user
   * @param  {Object} credentials The user credentials
   * @return {Promise} A promise to be fulfilled when the request is completed
   */
  createUser(credentials) {
    return new Promise((resolve, reject) => {
      this.ref.createUser(credentials, err => {
        err ? reject(err) : resolve(this.login(credentials));
      });
    });
  }
}

export default new LoginService(new Firebase(config.firebase));
