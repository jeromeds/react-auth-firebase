import React, { Component, PropTypes } from 'react';
import { Router, Route, IndexRoute } from 'react-router';
import { dispatch } from './AppDispatcher.js';
import ActionTypes from './constants/ActionTypes.js';
import * as PageConstants from './constants/PageConstants.js';
import importComponent from './utils/importComponent.js';
import { navigate } from './actions/NavsActionCreators.js';

import App from './components/App/App.js';
import IntroPage from './components/IntroPage/IntroPage.js';
import ForgetPasswordPage from './components/ForgetPasswordPage/ForgetPasswordPage.js';
import LoginPage from './components/LoginPage/LoginPage.js';
import SignupPage from './components/SignupPage/SignupPage.js';
import ProfilePage from './components/ProfilePage/ProfilePage.js';
import HomePage from './components/HomePage/HomePage.js';
import MessagesPage from './components/MessagesPage/MessagesPage.js';
import NotesHomePage from './components/NotesPage/NotesHomePage.js';
import NotesPage from './components/NotesPage/NotesPage.js';
import TalkPage from './components/TalkPage/TalkPage.js';
import AttendeesPage from './components/AttendeesPage/AttendeesPage.js';
import SpeakersPage from './components/SpeakersPage/SpeakersPage.js';
import SpeakerPage from './components/SpeakersPage/SpeakerPage.js';
import SchedulePage from './components/SchedulePage/SchedulePage.js';
import AuthStore from './stores/AuthStore.js';
import connectToStores from './utils/connectToStores.js';

// function getState() {
//   return {
//     authenticated: AuthStore.authenticated
//   };
// }
//
// @connectToStores([AuthStore], getState)


function requireAuth(nextState, replace) {
  if (!AuthStore.authenticated) {
    replace({
      pathname: '/login',
      state: { nextPathname: nextState.location.pathname }
    })
  }
}

/**
 * The root Liveshout Application component, performing routing
 */
export default class Application extends Component {

  displayName = 'LiveshoutApp';

  static propTypes = {
    history: PropTypes.object.isRequired,
    authenticated: PropTypes.bool
  };

  render() {
    return (
      <div>
        <Router history={this.props.history}>
          <Route path="/" component={App}>
            <IndexRoute name={PageConstants.pageNames.INTRO} component={IntroPage} />
            <Route name={PageConstants.pageNames.INTRO} path={PageConstants.pageNames.INTRO} component={IntroPage} />
            <Route name={PageConstants.pageNames.SIGNUP} path={PageConstants.pageNames.SIGNUP} component={SignupPage} />
            <Route name={PageConstants.pageNames.LOGIN} path={PageConstants.pageNames.LOGIN} component={LoginPage} />
            <Route name={PageConstants.pageNames.FORGOT_PASSWORD} path={PageConstants.pageNames.FORGOT_PASSWORD} component={ForgetPasswordPage} />
            <Route name={PageConstants.pageNames.PROFILE} path={PageConstants.pageNames.PROFILE} component={ProfilePage} />
            <Route name={PageConstants.pageNames.HOME} path={PageConstants.pageNames.HOME} component={HomePage} />
            <Route name={PageConstants.pageNames.SPEAKERS} path={PageConstants.pageNames.SPEAKERS} component={SpeakersPage} />
            <Route name={PageConstants.pageNames.SPEAKER} path={`${PageConstants.pageNames.SPEAKER}(/:id)`} component={SpeakerPage} />
            <Route name={PageConstants.pageNames.ATTENDEES} path={PageConstants.pageNames.ATTENDEES} component={AttendeesPage} />
            <Route name={PageConstants.pageNames.TALK}  path={`${PageConstants.pageNames.TALK}(/:id)`} component={TalkPage}/>
            <Route name={PageConstants.pageNames.MESSAGES} path={PageConstants.pageNames.MESSAGES} component={MessagesPage} onEnter={requireAuth} />
            <Route name={PageConstants.pageNames.NOTES} path={PageConstants.pageNames.NOTES} component={NotesHomePage} onEnter={requireAuth} />
            <Route name={PageConstants.pageNames.NOTE} path={`${PageConstants.pageNames.NOTE}(/:id)`} component={NotesPage} onEnter={requireAuth} />
            <Route name={PageConstants.pageNames.SCHEDULE} path={PageConstants.pageNames.SCHEDULE} component={SchedulePage} />
          </Route>
        </Router>
      </div>
    );
  }
}
