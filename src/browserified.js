/* globals exports, require */
// modules imported here will be available in the application via Browserify

exports.EventEmitter = require('events').EventEmitter;
exports.keymirror = require('keymirror');
exports.classNames = require('classnames');
exports.sprintf = require('sprintf-js').sprintf;
exports.shallowEqual = require('react-pure-render/shallowEqual');
exports.validator = require('validator');
