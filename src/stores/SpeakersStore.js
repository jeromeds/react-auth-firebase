import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';

class SpeakersStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._speakers = [];
    this._speaker = {};
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.REQUEST_SPEAKERS:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_SPEAKERS_SUCCESS:
        let response = action.response || null;
        this._status = null;

        // If data exist, convert the object into array
        if (response !== null) {
          this._speakers = Object.keys(response).map(key => {
            response[key].id = key;
            return response[key];
          });
        }

        this.emitChange();
        break;
      case ActionTypes.REQUEST_SPEAKERS_FAILURE:
      case ActionTypes.REQUEST_SPEAKER:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_SPEAKER_SUCCESS:
        this._speaker = action.response || {};
        if (action.response) {
          this._speaker.id = action.id;
        }
        this._status = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_SPEAKER_FAILURE:
      case ActionTypes.UPDATE_SPEAKER:
        this.mergeChanges(this._speaker, action.speaker);
        break;
      case ActionTypes.PUSH_SPEAKER:
        this._status = null;
        this._speaker = action.speaker;
        //if (this.validate()) {
          this._status = 'pending';
          this._error = null;
        //}
        this.emitChange();
        break;
      case ActionTypes.PUSH_SPEAKER_SUCCESS:
        this._status = 'success';
        this._speaker.id = action.response.id;
        this.emitChange();
        break;
      case ActionTypes.PUSH_SPEAKER_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.SAVE_SPEAKER:
        this._status = null;
        this._speaker = action.speaker;
        //if (this.validate()) {
          this._status = 'pending';
          this._error = null;
        //}
        this.emitChange();
        break;
      case ActionTypes.SAVE_SPEAKER_SUCCESS:
        this._status = 'success';
        this.emitChange();
        break;
      case ActionTypes.SAVE_SPEAKER_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.REMOVE_SPEAKER:
        this.emitChange();
        break;
      case ActionTypes.REMOVE_SPEAKER_SUCCESS:
        delete this._speakers[action.id];
        this.emitChange();
        break;
      case ActionTypes.REMOVE_SPEAKER_FAILURE:
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.RESET_SPEAKER:
        this._speaker = {};
        this.emitChange();
        break;
    }
  }

  find(filter) {
    if (this._speakers) {
      let speakersArr = [];
      // todo: improve this. It's really ugly. Find a better way to convert
      // the object in the response in an array.
      // for (let key in this._speakers) {
      //   this._speakers[key].id = key;
      //   speakersArr.push(this._speakers[key]);
      // }
      return speakersArr.filter(speaker => {
           if (!!filter && filter !== 'all') {
             return !!filter && speaker.status === filter;
           }
           return true;
         }
       );
    }
    return [];
  }

  /**
   * Get the list of speakers
   * @return {object} The list of speakers
   */
  get speakers() {
    return this._speakers;
  }

  get speaker() {
    return this._speaker;
  }
}

export default new SpeakersStore();
