import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';

/**
 * A store for managing profile state
 */
export default class ProfileStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._profile = {};
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.REQUEST_PROFILE:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_PROFILE_SUCCESS:
        this._profile = action.response || {};
        console.log(this._profile);
        this._status = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_PROFILE_FAILURE:
      case ActionTypes.UPDATE_PROFILE:
        this.mergeChanges(this._profile, action.profile);
        console.log(this._profile, action.profile);
        break;
      // case ActionTypes.SAVE_PROFILE:
      //   this._status = null;
      //   if (this.validate()) {
      //     this._status = 'pending';
      //     this._error = null;
      //   }
      //   this.emitChange();
      //   break;
      // case ActionTypes.SAVE_PROFILE_SUCCESS:
      //   this._status = 'success';
      //   this._profile = action.profile;
      //   this.emitChange();
      //   break;
      // case ActionTypes.SAVE_PROFILE_FAILURE:
      //   this._status = 'failure';
      //   this._error = action.error;
      //   this.emitChange();
      //   break;
    }
  }

  /**
   * Gets the profile properties
   * @return {Object} The profile properties
   */
  get profile() {
    return this._profile;
  }

  /**
   * Gets the current flow status
   * @return {string} One of "loading", "ready", "pending", "success" or "failure"
   */
  get status() {
    return this._status || 'ready';
  }

  /**
   * Gets the error if the update profile attempt failed
   * @return {Error} The error instance with a human-readable `code` property
   */
  get error() {
    return this._error;
  }

  /**
   * Gets the list of validation errors as an array of human-readable error codes
   * @return {[String]} The array of validation error codes
   */
  get validationErrors() {
    return this._validationErrors;
  }

  /**
   * Gets whether the store is currently valid
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  get isValid() {
    return !this._validationErrors;
  }

  /**
   * Performs validation of the store properties
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  validate() {
    const { name } = this._profile;
    let errors = [];
    if (!name || !name.trim()) {
      errors.push('NAME_REQUIRED');
    }
    this._validationErrors = errors.length ? errors : null;
    return this.isValid;
  }

}

export default new ProfileStore();
