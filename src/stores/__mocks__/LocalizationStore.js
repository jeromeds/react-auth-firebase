/* globals jest, module */

var store = jest.genMockFromModule('../LocalizationStore');

store.isReady = true;
store.get.mockReturnValue('localized');
store.translationHandler.mockReturnValue(key => `localized ${key}`);

module.exports = store;
