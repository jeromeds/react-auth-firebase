import { validator } from '../browserified.js';
import { passwordPolicy } from '../config.json';
import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';
import AuthStore from './AuthStore.js';

/**
 * A store for managing the change password and reset password workflows
 */
export default class ChangePasswordStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._clearCredentials();
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.UPDATE_PASSWORD_CREDENTIALS:
        this._validationErrors = null;
        this.mergeChanges(this._credentials, action.credentials);
        break;
      case ActionTypes.CHANGE_PASSWORD:
      case ActionTypes.RESET_PASSWORD:
        if (this.validate(action.type)) {
          this._status = 'pending';
          this._error = null;
        }
        this.emitChange();
        break;
      case ActionTypes.CHANGE_PASSWORD_SUCCESS:
      case ActionTypes.RESET_PASSWORD_SUCCESS:
        this._status = 'success';
        this._clearCredentials();
        this.emitChange();
        break;
      case ActionTypes.CHANGE_PASSWORD_FAILURE:
      case ActionTypes.RESET_PASSWORD_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.NAVIGATE:
        this._status = null;
        this._error = null;
        this._clearCredentials();
        this.emitChange();
        break;
    }
  }

  /**
   * Resets credentials to the initial state
   */
  _clearCredentials() {
    const { user } = AuthStore;
    this._credentials = {};
    if (user) {
      this._credentials.email = user.email;
    }
  }

  /**
   * Gets the credentials currently in use by the store
   * @return {Object} The credentials object
   */
  get credentials() {
    return this._credentials;
  }

  /**
   * Gets the current flow status
   * @return {string} One of "initial", "pending", "success" or "failure"
   */
  get status() {
    return this._status || 'initial';
  }

  /**
   * Gets the configured password policy
   * @return {Object} The password policy, if configured
   */
  get passwordPolicy() {
    return passwordPolicy;
  }

  /**
   * Gets the error if the reset password attempt failed
   * @return {Error} The error instance with a human-readable `code` property
   */
  get error() {
    return this._error;
  }

  /**
   * Gets the list of validation errors as an array of human-readable error codes
   * @return {[String]} The array of validation error codes
   */
  get validationErrors() {
    return this._validationErrors;
  }

  /**
   * Gets whether the store is currently valid
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  get isValid() {
    return !this._validationErrors;
  }

  /**
   * Performs validation of the store properties
   * @param  {Object} type The action type
   * @return {Boolean}     True if the store is currently in a valid state, false otherwise
   */
  validate(type) {
    let errors;
    if (type === ActionTypes.RESET_PASSWORD) {
      errors = this.validateResetPassword();
    } else if (type === ActionTypes.CHANGE_PASSWORD) {
      errors = this.validateChangePassword();
    }
    this._validationErrors = errors.length ? errors : null;
    return this.isValid;
  }

  /**
   * Validates the store for a reset password operation
   * @return {[String]} The array of validation error codes
   */
  validateResetPassword() {
    let errors = [];
    const { email } = this._credentials;
    if (!email) {
      errors.push('EMAIL_REQUIRED');
    }
    if (email && !/^[\w._%+-]+@[\w.-]+\.[A-Z]{2,}$/i.test(email)) {
      errors.push('ILLEGAL_EMAIL');
    }
    return errors;
  }

  /**
   * Validates the store for a change password operation
   * @return {[String]} The array of validation error codes
   */
  validateChangePassword() {
    const { oldPassword, newPassword, confirmPassword } = this._credentials;
    let errors = [];
    if (!oldPassword) {
      errors.push('OLD_PASSWORD_REQUIRED');
    }
    if (!newPassword) {
      errors.push('PASSWORD_TOO_SHORT');
    } else if (passwordPolicy && passwordPolicy.minLength &&
               !validator.isLength(newPassword, passwordPolicy.minLength)) {
      errors.push('PASSWORD_TOO_SHORT');
    }
    if (confirmPassword !== newPassword) {
      errors.push('PASSWORDS_DO_NOT_MATCH');
    }
    return errors;
  }

}

export default new ChangePasswordStore();
