import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';
import PageConstants from '../constants/PageConstants.js';
import * as AppConfig from '../constants/AppConfig.js';

class NavsStore extends BaseStore {
  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._isLeftNav = false;
    this._isRightNav = false;
    this._currentPage = {
      parent: null,
      name: null,
      group: null
    };
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.TOGGLE_LEFT_NAV:
        this._isLeftNav = !this._isLeftNav;
        this.emitChange();
        break;
      case ActionTypes.TOGGLE_RIGHT_NAV:
        this._isRightNav = !this._isRightNav;
        this.emitChange();
        break;
      case ActionTypes.NAVIGATE:
      console.log('update current page');
        this._currentPage.name = action.name;
        this._currentPage.group = this._getGroup(action.name);
        this._currentPage.parent = this._getParent(action.name);
        this.emitChange();
      break;
    }
  }

  _getGroup(name) {
    // TODO: refactor this
    switch (name) {
      case PageConstants.pageNames.ATTENDEE:
        return PageConstants.pageNames.ATTENDEES;
      case PageConstants.pageNames.SPEAKER:
        return PageConstants.pageNames.SPEAKERS;
      case PageConstants.pageNames.TALK:
        return PageConstants.pageNames.TALKS;
      case PageConstants.pageNames.MESSAGE:
        return PageConstants.pageNames.MESSAGES;
      case PageConstants.pageNames.NOTE:
        return PageConstants.pageNames.NOTES;
    }
    return name;
  }

  _getParent(name) {
    // TODO: refactor this
    switch (name) {
      case PageConstants.pageNames.ATTENDEE:
        return PageConstants.pageNames.ATTENDEES;
      case PageConstants.pageNames.SPEAKER:
        return PageConstants.pageNames.SPEAKERS;
      case PageConstants.pageNames.TALK:
        return PageConstants.pageNames.TALKS;
      case PageConstants.pageNames.MESSAGE:
        return PageConstants.pageNames.MESSAGES;
      case PageConstants.pageNames.NOTE:
        return PageConstants.pageNames.NOTES;
    }
    return null;
  }

  get isLeftNav() {
    return this._isLeftNav;
  }

  get isRightNav() {
    return this._isRightNav;
  }

  get currentPage() {
    return this._currentPage;
  }

  get showRightNav() {
    // const showRightNav = AppConfig.pageParams.filter(x => x.rightNav && x.name === this._currentPage.name);
    // if (showRightNav.length) {
    //   return true;
    // }
    return false;
  }
}

export default new NavsStore();
