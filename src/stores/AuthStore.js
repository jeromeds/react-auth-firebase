import { shallowEqual } from '../browserified.js';
import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';
import api from '../api/LoginService.js';

/**
 * A store for managing the authentication state
 */
class AuthStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._handleAuthEvents();
    this._auth = api.current();
  }

  _handleAuthEvents() {
    // use Firebase events instead of dispatched actions for maximum reliability
    api.on('login', auth => {
      this._auth = auth;
      this.emitChange();
    }).on('logout', () => {
      this._auth = null;
      this.emitChange();
    });
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.CHANGE_PASSWORD_SUCCESS:
        this.user.isTemporaryPassword = false;
        this.emitChange();
        break;
    }
  }

  /**
   * Gets the user currently logged in
   * @return {Object} The user details, or null if user is not logged in
   */
  get user() {
    if (this._auth) {
      let providerProps = this._auth[this.auth.provider];
      return {
        uid: this._auth.uid,
        provider: this._auth.provider,
        expires: this._auth.expires,
        ...providerProps
      };
    }
    return null;
  }

  /**
   * Gets the current authentication details
   * @return {Object} The authentication details, or null if user is not logged in
   */
  get auth() {
    return this._auth ? this._auth.auth : null;
  }

  /**
   * Gets whether the user is currently authenticated
   * @return {Boolean} True if the user is logged in, false otherwise
   */
  get authenticated() {
    return !!this._auth;
  }

}

export default new AuthStore();
