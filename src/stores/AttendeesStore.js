import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';

class AttendeesStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._attendees = [];
    this._attendee = {};
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.REQUEST_ATTENDEES:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_ATTENDEES_SUCCESS:
        let response = action.response || null;
        this._status = null;
        // If data exist, convert the object into array
        if (response !== null) {
          this._attendees = Object.keys(response).map(key => {
            response[key].id = key;
            return response[key];
          });
        }
        this.emitChange();
        break;
      case ActionTypes.REQUEST_ATTENDEES_FAILURE:
      case ActionTypes.REQUEST_ATTENDEE:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_ATTENDEE_SUCCESS:
        this._attendee = action.response || null;
        this._attendee.id = action.id;
        this._status = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_ATTENDEE_FAILURE:
      case ActionTypes.UPDATE_ATTENDEE:
        this.mergeChanges(this._attendee, action.attendee);
        break;
      case ActionTypes.PUSH_ATTENDEE:
        this._status = null;
        this._attendee = action.attendee;
        //if (this.validate()) {
          this._status = 'pending';
          this._error = null;
        //}
        this.emitChange();
        break;
      case ActionTypes.PUSH_ATTENDEE_SUCCESS:
        this._status = 'success';
        this._attendee.id = action.response.id;
        this.emitChange();
        break;
      case ActionTypes.PUSH_ATTENDEE_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.SAVE_ATTENDEE:
        this._status = null;
        this._attendee = action.attendee;
        //if (this.validate()) {
          this._status = 'pending';
          this._error = null;
        //}
        this.emitChange();
        break;
      case ActionTypes.SAVE_ATTENDEE_SUCCESS:
        this._status = 'success';
        this.emitChange();
        break;
      case ActionTypes.SAVE_ATTENDEE_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.REMOVE_ATTENDEE:
        this.emitChange();
        break;
      case ActionTypes.REMOVE_ATTENDEE_SUCCESS:
        delete this._attendees[action.id];
        this.emitChange();
        break;
      case ActionTypes.REMOVE_ATTENDEE_FAILURE:
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.RESET_ATTENDEE:
        this._attendee = {};
        this.emitChange();
        break;
    }
  }

  find(filter) {
    if (this._attendees) {
      let attendeesArr = [];
      // todo: improve this. It's really ugly. Find a better way to convert
      // the object in the response in an array.
      // for (let key in this._attendees) {
      //   this._attendees[key].id = key;
      //   attendeesArr.push(this._attendees[key]);
      // }
      return attendeesArr.filter(attendee => {
           if (!!filter && filter !== 'all') {
             return !!filter && attendee.status === filter;
           }
           return true;
         }
       );
    }
    return [];
  }

  /**
   * Get the list of attendees
   * @return {object} The list of attendees
   */
  get attendees() {
    return this._attendees;
  }

  get attendee() {
    return this._attendee;
  }
}

export default new AttendeesStore();
