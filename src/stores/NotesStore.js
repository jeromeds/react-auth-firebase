import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';
import { push } from '../../actions/ChoicesActionCreators.js';

/**
 * A store for managing the login and logout workflows
 */
class NotesStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._notes = null;
    this._note = {};
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.REQUEST_NOTES:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_NOTES_SUCCESS:
        let response = action.response || null;
        if (response !== null) {
          this._notes = response;
          // this._notes = Object.keys(response).map(key => {
          //   response[key].id = key;
          //   // response[key].notes = Object.keys(response[key]).map(key2 => {
          //   //   return response[key][key2];
          //   // });
          //   return response[key];
          // });



          //console.log(json_decode(json_encode(response), true));
          // Sort them by date
          //this.sortNotes();
        }

        this._status = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_NOTES_FAILURE:
      case ActionTypes.REQUEST_NOTE:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_NOTE_SUCCESS:
        this._note = action.response || {};
        this._status = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_NOTE_FAILURE:
        this._note = {};
        this.emitChange();
        break;
      case ActionTypes.PUSH_NOTE:
        this._status = null;
        this._note = action.note;
        // if (this.validate()) {
        //   this._status = 'pending';
        //   this._error = null;
        // }
        this.emitChange();
        break;
      case ActionTypes.PUSH_NOTE_SUCCESS:
        const { type, dateTime } = action.note;
        this._notes.push({
          id: action.response.id,
          text: type,
          type: type,
          dateTime
        });
        this._status = 'success';
        this.emitChange();
        break;
      case ActionTypes.PUSH_NOTE_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.UPDATE_NOTE:
        this.mergeChanges(this._notes[action.index], action.note);
        this.emitChange();
        break;
      case ActionTypes.SAVE_NOTE:
        this._status = null;
        // if (this.validate()) {
        //   this._status = 'pending';
        //   this._error = null;
        // }
        this.emitChange();
        break;
      case ActionTypes.SAVE_NOTE_SUCCESS:
        this._status = 'success';
        this._note = action.note;
        this.sortNotes();
        setTimeout(function () {
          push(action.note.id);
        }, 0);
        break;
      case ActionTypes.SAVE_NOTE_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.REMOVE_NOTE:
        this.emitChange();
        break;
      case ActionTypes.REMOVE_NOTE_SUCCESS:
        this._notes.splice(action.noteIndex, 1);
        this.emitChange();
        break;
      case ActionTypes.REMOVE_NOTE_FAILURE:
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.EDIT_NOTE:
        this._error = action.error;
        this._notes[action.noteIndex].isEditable = !this._notes[action.noteIndex].isEditable;
        this.emitChange();
        break;
    }
  }

  sortNotes() {
    this._notes.sort(function (a, b) {
      if (a.dateTime > b.dateTime) {
        return 1;
      }
      if (a.dateTime < b.dateTime) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });
  }

  isNoteEditable(noteIndex) {
    return this._notes[noteIndex].isEditable;
  }

  /**
   * Get the list of notes
   * @return {object} The list of notes
   */
  get notes() {
    //todo: organize them by date
    return this._notes;
  }

  /**
   * Get the note properties
   * @return {Object} The note properties
   */
  note(index) {
    return this._notes[index];
  }

  /**
   * Gets the current flow status
   * @return {string} One of "loading", "ready", "pending", "success" or "failure"
   */
  get status() {
    return this._status || 'ready';
  }

  /**
   * Gets the error if the update note attempt failed
   * @return {Error} The error instance with a human-readable `code` property
   */
  get error() {
    return this._error;
  }

  /**
   * Gets the list of validation errors as an array of human-readable error codes
   * @return {[String]} The array of validation error codes
   */
  get validationErrors() {
    return this._validationErrors;
  }

  /**
   * Gets whether the store is currently valid
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  get isValid() {
    return !this._validationErrors;
  }

  /**
   * Performs validation of the store properties
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  validate() {
    const { name } = this._note;
    let errors = [];
    if (!name || !name.trim()) {
      errors.push('NAME_REQUIRED');
    }
    this._validationErrors = errors.length ? errors : null;
    return this.isValid;
  }
}

export default new NotesStore();
