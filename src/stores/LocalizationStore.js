import BaseStore from './BaseStore.js';
import Localization from '../api/LocalizationService.js';

/**
 * A store for managing localization state
 */
class LocalizationStore extends BaseStore {

  constructor() {
    super();
    this._initialized = false;
  }

  /**
   * Loads localization data from a JSON file
   * @param  {string} [path] The path of the JSON file to load
   * @return {Promise}       A promise fulfilled when localization data has been loaded
   */
  load(path) {
    return Localization.load(path).then(localization => {
      this._localization = localization;
      this._initialized = true;
      this.emitChange();
    });
  }

  /**
   * Gets the currently loaded localization data
   * @return {Object} Localization data loaded from the JSON file
   */
  get localization() {
    return this._localization;
  }

  /**
   * Gets whether localization data is currently available
   * @return {Boolean} True is localization data is available, false otherwise
   */
  get isReady() {
    return this._initialized && this._localization;
  }

  /**
   * Retrieves localization at the given path
   * @param  {string} [path] If specified, the path of the localization data to retrieve
   * @return {Object|string} The localization data found at `path`
   */
  get(path) {
    if (!path) return this.localization;
    return Localization.translate(this.localization, path);
  }

  /**
   * Returns a helper function to retrieving localization at a given path
   * @param  {string} [path] If specified, the path of the localization data to retrieve
   * @return {Function}      A function for quickly retrieving localization data
   */
  translationHandler(path) {
    if (this.isReady) {
      return Localization.translate.bind(null, this.get(path));
    } else {
      return function t(key) {
        if (path) return `${path}.${key}`;
        return key;
      }
    }

  }

}

export default new LocalizationStore();
