import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';
import moment from 'moment';
/**
 * A store for managing the login and logout workflows
 */
class TalksStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._talks = [];
    this._talk = {};
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.REQUEST_TALKS:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_TALKS_SUCCESS:
        let response = action.response || null;
        this._status = null;

        // If data exist, convert the object into array
        if (response !== null) {
          this._talks = Object.keys(response).map(key => {
            response[key].id = key;
            return response[key];
          });
        }

        this.emitChange();
        break;
      case ActionTypes.REQUEST_TALKS_FAILURE:
      case ActionTypes.REQUEST_TALK:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_TALK_SUCCESS:
        this._talk = action.response || {};
        if (action.response) {
          this._talk.id = action.id;
        }
        this._status = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_TALK_FAILURE:
        this._talk = {};
        this.emitChange();
        break;
      case ActionTypes.PUSH_TALK:
        this._status = null;
        this._talk = action.talk;
        if (this.validate()) {
          this._status = 'pending';
          this._error = null;
        }
        this.emitChange();
        break;
      case ActionTypes.PUSH_TALK_SUCCESS:
        this._status = 'success';
        this._talk.id = action.response.id;
        this.emitChange();
        break;
      case ActionTypes.PUSH_TALK_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.UDPATE_TALK:
        if (action.talk.hasOwnProperty('startingDate')) {
          let startingHour = this.talk.startingHour;
          startingHour = ('0' + startingHour).slice(-2);
          //todo: find a better way to do this... moment must have a function
          //to set the hour directly
          action.talk.starting = this.dateToTimestamp(action.talk.startingDate.format(`YYYY-MM-DD ${startingHour}:00:00`));
        }
        if (action.talk.hasOwnProperty('endingDate')) {
          let endingHour = this.talk.endingHour;
          endingHour = ('0' + endingHour).slice(-2);
          action.talk.ending = this.dateToTimestamp(action.talk.endingDate.format(`YYYY-MM-DD ${endingHour}:00:00`));
        }
        this.mergeChanges(this._talk, action.talk);
        break;
      case ActionTypes.SAVE_TALK:
        this._status = null;
        if (this.validate()) {
          this._status = 'pending';
          this._error = null;
        }
        this.emitChange();
        break;
      case ActionTypes.SAVE_TALK_SUCCESS:
        this._status = 'success';
        this._talk = action.talk;
        this.emitChange();
        break;
      case ActionTypes.SAVE_TALK_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.REMOVE_TALK:
        this.emitChange();
        break;
      case ActionTypes.REMOVE_TALK_SUCCESS:
        delete this._talks[action.id];
        this.emitChange();
        break;
      case ActionTypes.REMOVE_TALK_FAILURE:
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.RESET_TALK:
        this._talk = {};
        this.emitChange();
        break;
      case ActionTypes.ADD_TALK_SPEAKER:
        if (!this._talk.hasOwnProperty('speakers')) {
          this._talk.speakers = [];
        }
        this._talk.speakers.push(action.id);
        this.emitChange();
        break;
      case ActionTypes.REMOVE_TALK_SPEAKER:
        let speakers = this._talk.speakers;
        const index = speakers.indexOf(action.id);
        if (index > -1) {
          speakers.splice(index, 1);
        }
        this._talk.speakers = speakers;
        this.emitChange();
        break;
    }
  }

  find(filter) {
    if (this._talks) {
      let talksArr = [];
      // todo: improve this. It's really ugly. Find a better way to convert
      // the object in the response in an array.
      for (let key in this._talks) {
        this._talks[key].id = key;
        talksArr.push(this._talks[key]);
      }
      return talksArr.filter(talk => {
           if (!!filter && filter !== 'all') {
             return !!filter && talk.status === filter;
           }
           return true;
         }
       );
    }
    return [];
  }

  dateToTimestamp(date) {
    return moment(date).unix();
  }

  /**
   * Get the list of talks
   * @return {object} The list of talks
   */
  get talks() {
    return this._talks;
  }

  /**
   * Get the talk properties
   * @return {Object} The talk properties
   */
  get talk() {
    return this._talk;
  }

  get isTalk() {
    return !!Object.keys(this._talk).length;
  }

  /**
   * Get the talk properties
   * @return {Object} The talk properties
   */
  get isTalkEmpty() {
    return Object.keys(this._talk).length ? false : true;
  }

  /**
   * Gets the current flow status
   * @return {string} One of "loading", "ready", "pending", "success" or "failure"
   */
  get status() {
    return this._status || 'ready';
  }

  /**
   * Gets the error if the update talk attempt failed
   * @return {Error} The error instance with a human-readable `code` property
   */
  get error() {
    return this._error;
  }

  /**
   * Gets the list of validation errors as an array of human-readable error codes
   * @return {[String]} The array of validation error codes
   */
  get validationErrors() {
    return this._validationErrors;
  }

  /**
   * Gets whether the store is currently valid
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  get isValid() {
    return !this._validationErrors;
  }

  /**
   * Performs validation of the store properties
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  validate() {
    const { name } = this._talk;
    let errors = [];
    if (!name || !name.trim()) {
      errors.push('NAME_REQUIRED');
    }
    this._validationErrors = errors.length ? errors : null;
    return this.isValid;
  }
}

export default new TalksStore();
