import { validator } from '../browserified.js';
import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';
import { createUser, createProfile } from '../actions/LoginActionCreators.js';
import { updateProfile } from '../actions/ProfileActionCreators.js';

/**
 * A store for managing the login and logout workflows
 */
class LoginStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._clearCredentials();
  }

  _registerToActions(action) {
    switch (action.type) {

      case ActionTypes.UPDATE_LOGIN_CREDENTIALS:
        this._validationErrors = null;
        this.mergeChanges(this._credentials, action.credentials);
        break;
      case ActionTypes.UPDATE_LOGIN_OPTIONS:
        this.mergeChanges(this._options, action.options);
        break;
      case ActionTypes.LOGIN:
        if (this.validate()) {
          this._isLoggingIn = true;
          this._error = null;
        }
        this.emitChange();
        break;
      case ActionTypes.LOGIN_SUCCESS:
        this._isLoggingIn = false;
        this._clearCredentials();
        window.location = '/home';
        this.emitChange();
        break;
      case ActionTypes.LOGIN_FAILURE:
        this._isLoggingIn = false;
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.VALIDATE_EMAIL:
      break;
      case ActionTypes.VALIDATE_EMAIL_SUCCESS:
      console.log(action.response);
        if (!!action.response) {
          setTimeout(() => {
            updateProfile(action.response);
          }, 1);
          setTimeout(() => {
            createUser(action.response);
          }, 1);
        } else {
          this._error = {message: 'Email do not exist in the admin'};
          this.emitChange();
        }
      break;
      case ActionTypes.VALIDATE_EMAIL_FAILURE:
      break;
      case ActionTypes.CREATE_USER:
        //if (this.validate()) {
          this._error = null;
        //}
        this.emitChange();
        break;
      case ActionTypes.CREATE_USER_SUCCESS:
        //todo: this is really ugly
        setTimeout(() => {
          createProfile();
        }, 1);
        this.emitChange();
        break;
      case ActionTypes.CREATE_USER_FAILURE:
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.CREATE_PROFILE:
        break;
      case ActionTypes.CREATE_PROFILE_SUCCESS:
        this._clearCredentials();
        this.emitChange();
        break;
      case ActionTypes.CREATE_PROFILE_FAILURE:
        break;
    }
  }

  /**
   * Resets both credentials and login options to the initial state
   */
  _clearCredentials() {
    this._credentials = {};
    this._options = { remember: true };
  }

  /**
   * Gets the current credentials in use with the store
   * @return {Object} The credentials object
   */
  get credentials() {
    return this._credentials;
  }

  /**
   * Gets the current credentials in use with the store
   * @return {Object} The credentials object
   */
  get profile() {
    return this._credentials;
  }

  /**
   * Gets the current login options
   * @return {Object} The login options to forward to the login API
   */
  get options() {
    return this._options;
  }

  /**
   * Gets the error if the login attempt resulted in a failure
   * @return {Error} The error instance, with a human-readable `code` property
   */
  get error() {
    return this._error;
  }

  /**
   * Gets whether a loggin attempt is currently in progress
   * @return {Boolean} True if the login attempt is still pending, false otherwise
   */
  get isLoggingIn() {
    return this._isLoggingIn || false;
  }

  /**
   * Gets the list of validation errors as an array of human-readable error codes
   * @return {[String]} The array of validation error codes
   */
  get validationErrors() {
    return this._validationErrors;
  }

  /**
   * Gets whether the store is currently valid
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  get isValid() {
    return !this._validationErrors;
  }

  /**
   * Performs validation of the store properties
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  validate() {
    const { email, password } = this._credentials;
    let errors = [];
    if (!email) {
      errors.push('EMAIL_REQUIRED');
    }
    if (email && !validator.isEmail(email)) {
      errors.push('ILLEGAL_EMAIL');
    }
    if (!password) {
      errors.push('PASSWORD_REQUIRED');
    }
    this._validationErrors = errors.length ? errors : null;
    return this.isValid;
  }

}

export default new LoginStore();
