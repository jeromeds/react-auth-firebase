import { EventEmitter, shallowEqual } from '../browserified.js';
import { register } from '../AppDispatcher.js';

/**
 * A base class for stores
 */
export default class BaseStore extends EventEmitter {

  /**
   * Subscribes an action handler bound to this store
   * @param  {Function} handler The handler to register
   * @return {Object}           The dispatch token
   */
  subscribe(handler) {
    this._dispatchToken = register(handler);
    return this._dispatchToken;
  }

  /**
   * Gets the dispatch token associated with this store
   * @return {Object} The dispatch token associated via `subscribe`
   */
  get dispatchToken() {
    return this._dispatchToken;
  }

  /**
   * Emits a `change` event
   */
  emitChange() {
    this.emit('change');
  }

  /**
   * Attaches a callback fired when the store changes
   * @param {Function} callback The store change callback
   */
  addChangeListener(callback) {
    this.on('change', callback)
  }

  /**
   * Detaches a callback previously attached to store changes
   * @param  {Function} callback The store change callback
   */
  removeChangeListener(callback) {
    this.removeListener('change', callback);
  }

  /**
   * Registers a callback which will fire at the next change
   * @param  {Function} callback The callback function to invoke once upon change
   */
  waitForChange(callback) {
    this.once('change', callback);
  }

  /**
   * Merges properties into a store bag, emitting a change when necessary
   * @param  {Object} target  The store bag to update
   * @param  {Object} changes The properties to merge into the bag
   */
  mergeChanges(target, changes) {
    let merged = Object.assign({ ...target }, changes);
    if (!shallowEqual(merged, target)) {
      Object.assign(target, merged);
      this.emitChange();
    }
  }
}
