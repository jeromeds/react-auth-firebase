import BaseStore from './BaseStore.js';
import ActionTypes from '../constants/ActionTypes.js';
/**
 * A store for managing the login and logout workflows
 */
class NotesStore extends BaseStore {

  constructor() {
    super();
    this.subscribe(this._registerToActions.bind(this));
    this._choices = [];
    this._choice = {};
  }

  _registerToActions(action) {
    switch (action.type) {
      case ActionTypes.REQUEST_CHOICES:
        this._status = 'loading';
        this._error = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_CHOICES_SUCCESS:
        let response = action.response || null;
        if (response) {
          this._choices = Object.keys(response).map(function (key) {
            response[key].id = key;
            return response[key];
          });
        }
        this._status = null;
        this.emitChange();
        break;
      case ActionTypes.REQUEST_CHOICES_FAILURE:
      case ActionTypes.PUSH_CHOICE:
        this._status = null;
        //this._note = action.note;
        // if (this.validate()) {
        //   this._status = 'pending';
        //   this._error = null;
        // }
        this.emitChange();
        break;
      case ActionTypes.PUSH_CHOICE_SUCCESS:
        this._choices.push({
          id: action.response.id,
          name: action.choice.name
        });
        this._status = 'success';
        this.emitChange();
        break;
      case ActionTypes.PUSH_CHOICE_FAILURE:
        this._status = 'failure';
        this._error = action.error;
        this.emitChange();
        break;
      case ActionTypes.ADD_CHOICE:
        this._choices.push(action.choice);
        this.emitChange();
        break;
      case ActionTypes.UPDATE_CHOICE:
        this.mergeChanges(this._choices[action.index], action.choice);
        this.emitChange();
        // what type
        break;
      case ActionTypes.REMOVE_CHOICE:
        this.emitChange();
        break;
      // case ActionTypes.REMOVE_CHOICE_SUCCESS:
      //   this._choices = this._choices.filter(choice => choice.id !== action.choiceId);
      //   this.emitChange();
      //   break;
      // case ActionTypes.REMOVE_CHOICE_FAILURE:
      //   this._error = action.error;
      //   this.emitChange();
      //   break;
    }
  }

  /**
   * Get the list of notes
   * @return {object} The list of notes
   */
  choices() {
    //todo: organize them by date
    return this._choices;
  }

  /**
   * Get the note properties
   * @return {Object} The note properties
   */
  choice(index) {
    return this._choices[index];
  }

  /**
   * Gets the current flow status
   * @return {string} One of "loading", "ready", "pending", "success" or "failure"
   */
  get status() {
    return this._status || 'ready';
  }

  /**
   * Gets the error if the update note attempt failed
   * @return {Error} The error instance with a human-readable `code` property
   */
  get error() {
    return this._error;
  }

  /**
   * Gets the list of validation errors as an array of human-readable error codes
   * @return {[String]} The array of validation error codes
   */
  get validationErrors() {
    return this._validationErrors;
  }

  /**
   * Gets whether the store is currently valid
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  get isValid() {
    return !this._validationErrors;
  }

  /**
   * Performs validation of the store properties
   * @return {Boolean} True if the store is currently in a valid state, false otherwise
   */
  validate() {
    const { name } = this._note;
    let errors = [];
    if (!name || !name.trim()) {
      errors.push('NAME_REQUIRED');
    }
    this._validationErrors = errors.length ? errors : null;
    return this.isValid;
  }
}

export default new NotesStore();
