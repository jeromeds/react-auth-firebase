jest.dontMock('../AppDispatcher.js');

let FluxDispatcher;
let AppDispatcher;
let dispatcher;

beforeEach(function () {
  FluxDispatcher = require('flux').Dispatcher;
  AppDispatcher = require('../AppDispatcher.js');
  dispatcher = FluxDispatcher.mock.instances[0];
});

describe("register", function () {
  it("should register a handler with Flux dispatcher", function () {
    let handler = () => null;
    AppDispatcher.register(handler);
    expect(dispatcher.register).toBeCalledWith([handler]);
  });
  it("should register a handler for a specific type", function () {
    AppDispatcher.register('TEST_ACTION', () => null);
    expect(dispatcher.register).toBeCalled();
  });
});

describe("dispatch", function () {
  it("should dispatch payload with type", function () {
    let payload = { test: true };
    AppDispatcher.dispatch('TEST_ACTION', payload);
    expect(dispatcher.dispatch).toBeCalledWith({
      type: 'TEST_ACTION',
      test: true
    });
  });
  it("should fail if type is missing", function () {
    expect(() => AppDispatcher.dispatch()).toThrow();
  });
  it("should fail to dispatch payload with a `type` property", function () {
    let payload = { type: 'AAA', test: true };
    expect(() => AppDispatcher.dispatch('TEST_ACTION', payload)).toThrow();
  });
});

describe("dispatchAsync", function () {
  it("should dispatch request at first", function () {
    let p = new Promise((resolve, reject) => null);
    let payload = { test: true };
    AppDispatcher.dispatchAsync(p, {
      request: 'REQUEST_TEST',
      success: 'TEST_SUCCESS',
      failure: 'TEST_FAILURE'
    }, payload);
    expect(dispatcher.dispatch).toBeCalledWith({
      type: 'REQUEST_TEST',
      test: true
    });
  });
  pit("should dispatch success with `response` payload when promise is resolved", function () {
    let p = Promise.resolve({ completed: true });
    AppDispatcher.dispatchAsync(p, {
      success: 'TEST_SUCCESS'
    });
    return p.then(() => {
      expect(dispatcher.dispatch).lastCalledWith({
        type: 'TEST_SUCCESS',
        response: {
          completed: true
        }
      });
    });
  });
  pit("should dispatch failure with `error` payload when promise is rejected", function () {
    let p = Promise.reject({ completed: false });
    AppDispatcher.dispatchAsync(p, {
      failure: 'TEST_FAILURE'
    });
    return p.then(null, () => {
      expect(dispatcher.dispatch).lastCalledWith({
        type: 'TEST_FAILURE',
        error: {
          completed: false
        }
      });
    });
  });
});
