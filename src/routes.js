/**
 * React Starter Kit (https://www.reactstarterkit.com/)
 *
 * Copyright © 2014-2016 Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import Router from 'react-routing/src/Router';
import fetch from './core/fetch';
import App from './components/App';
import HomePage from './components/HomePage/HomePage.js';
import MessagesPage from './components/MessagesPage/MessagesPage.js';
import MessagePage from './components/MessagesPage/MessagePage.js';
import NotesHomePage from './components/NotesPage/NotesHomePage.js';
import NotesPage from './components/NotesPage/NotesPage.js';
import TalksPage from './components/TalksPage/TalksPage.js';
import TalkPage from './components/TalksPage/TalkPage.js';
import AttendeesPage from './components/AttendeesPage/AttendeesPage.js';
import AttendeePage from './components/AttendeesPage/AttendeePage.js';
import SpeakersPage from './components/SpeakersPage/SpeakersPage.js';
import SpeakerPage from './components/SpeakersPage/SpeakerPage.js';


const router = new Router(on => {
  on('*', async (state, next) => {
    const component = await next();
    return component && <App context={state.context}>{component}</App>;
  });

  on('/contact', async () => <ContactPage />);

  on('/login', async () => <LoginPage />);

  on('/register', async () => <RegisterPage />);

  on('*', async (state) => {
    const response = await fetch(`/api/content?path=${state.path}`);
    const content = await response.json();
    return content && <ContentPage {...content} />;
  });

  on('error', (state, error) => state.statusCode === 404 ?
    <App context={state.context} error={error}><NotFoundPage /></App> :
    <App context={state.context} error={error}><ErrorPage /></App>
  );
});

export default router;
