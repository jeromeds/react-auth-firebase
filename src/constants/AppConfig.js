import { pageNames } from './PageConstants.js';

// Default value
// topNav {bool} false
// leftNav {bool} false
// rightNav {bool} false
// icon {bool} undefined

const pageParams = {
  [pageNames.INTRO]: {
  },
  [pageNames.LOGIN]: {
  },
  [pageNames.SIGNUP]: {
    topNav: true,
    leftNav: true
  },
  [pageNames.PROFILE]: {
    topNav: true,
    leftNav: true,
    rightNav: false
  },
  [pageNames.HOME]: {
    topNav: true,
    leftNav: true,
    rightNav: false,
    icon: 'more'
  },
  [pageNames.MESSAGES]: {
    topNav: true,
    leftNav: true,
    rightNav: true,
    icon: 'messages'
  },
  [pageNames.NOTES]: {
    topNav: true,
    leftNav: true,
    rightNav: true,
    icon: 'note'
  },
  [pageNames.ATTENDEES]: {
    topNav: true,
    leftNav: false,
    icon: 'persone'
  },
  [pageNames.SPEAKERS]: {
    topNav: true,
    leftNav: true,
    icon: 'persone'
  },
  [pageNames.SPEAKER]: {
    topNav: true,
    leftNav: true
  },
  [pageNames.SCHEDULE]: {
    topNav: true,
    leftNav: true,
    icon: 'time'
  },
  [pageNames.TALK]: {
    topNav: true,
    leftNav: true
  }
};

export default { pageParams };
