const pageNames = {
  DEFAULT: 'default',
  INTRO: 'intro',
  FORGOT_PASSWORD: 'forgot-password',
  SIGNUP: 'signup',
  LOGIN: 'login',
  PROFILE: 'profile',
  HOME: 'home',
  NOTES: 'notes',
  NOTE: 'note',
  MESSAGES: 'messages',
  MESSAGE: 'message',
  MY_PROFILE: 'my-profile',
  TALKS: 'talks',
  TALK: 'talk',
  ATTENDEES: 'attendees',
  ATTENDEE: 'attendee',
  SPEAKERS: 'speakers',
  SPEAKER: 'speaker',
  SCHEDULE: 'schedule'
};

const pageParent = {
  [pageNames.LOGIN]: [pageNames.INTRO],
  [pageNames.SIGNUP]: [pageNames.INTRO],
  [pageNames.NOTE]: [pageNames.NOTES],
  [pageNames.ATTENDEE]: [pageNames.ATTENDEES],
  [pageNames.TALK]: [pageNames.SCHEDULE],
  [pageNames.SPEAKER]: [pageNames.SPEAKERS],
  [pageNames.MESSAGE]: [pageNames.MESSAGES]
};

export default { pageNames, pageParent };
