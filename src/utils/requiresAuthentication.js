import React, { Component } from 'react';
import store from '../stores/AuthStore.js';
import importComponent from './importComponent.js';

/**
 * Prevents a component from rendering for unauthenticated users
 * @param  {Component} [component]  The component to render if the user is not logged in
 * @return {Component}              A component that will render differently if the user is not logged in
 */
export default function requiresAuthentication(component) {

  if (!component) {
    // use the default login page
    component = importComponent('/components/LoginPage/LoginPage.js');
  }

  return function (DecoratedComponent) {
    return class AuthenticationProxy extends Component {

      static displayName = 'requiresAuthentication';

      constructor(props) {
        super(props);
        this.handleLoginChanged = this.handleLoginChanged.bind(this);
        this.state = {
          authenticated: store.authenticated,
          user: store.user
        };
      }

      componentWillMount() {
        store.addChangeListener(this.handleLoginChanged);
      }

      componentWillUnmount() {
        store.removeChangeListener(this.handleLoginChanged);
      }

      handleLoginChanged() {
        this.setState({
          authenticated: store.authenticated,
          user: store.user
        });
      }

      render() {
        const { authenticated, user } = this.state;
        if (!authenticated) {

          //return null;
          return React.createElement(component, this.state);
        }
        return <DecoratedComponent {...this.props} user={user} />;
      }
    };
  };
}
