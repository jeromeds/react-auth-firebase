import React, { Component } from 'react';
import { shallowEqual } from '../browserified.js';

/**
 * Provides a component with the ability to watch for changes on stores and update accordingly
 * @param  {Object[]} stores   The list of stores to watch for changes
 * @param  {Function} getState A function returning the state to use as props for the decorated component
 * @return {Component}         A component that will watch store changes, update its state, then pass it as props to the decorated component
 */
export default function connectToStores(stores, getState) {

  getState = getState || function () {
    return {};
  };

  return function (DecoratedComponent) {
    return class StoreConnector extends Component {

      static displayName = `connectToStores`;

      constructor(props) {
        super(props);
        this.handleStoresChanged = this.handleStoresChanged.bind(this);
        this.state = getState(props);
      }

      componentWillMount() {
        stores.forEach(store =>
          store.addChangeListener(this.handleStoresChanged)
        );
      }

      componentWillReceiveProps(nextProps) {
        if (!shallowEqual(nextProps, this.props)) {
          this.setState(getState(nextProps));
        }
      }

      componentWillUnmount() {
        stores.forEach(store =>
          store.removeChangeListener(this.handleStoresChanged)
        );
      }

      handleStoresChanged() {
        this.setState(getState(this.props));
      }

      render() {
        return <DecoratedComponent {...this.props} {...this.state} />;
      }
    };
  };
}
