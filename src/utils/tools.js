import AppConfig from '../../constants/AppConfig.js';

const isTopNav = (pageName) => {
  return AppConfig.pageParams.hasOwnProperty(pageName) && AppConfig.pageParams[pageName].topNav;
};

const isLeftNav = (pageName) => {
  return AppConfig.pageParams.hasOwnProperty(pageName) && AppConfig.pageParams[pageName].leftNav;
};

const isRightNav = (pageName) => {
  return AppConfig.pageParams.hasOwnProperty(pageName) && AppConfig.pageParams[pageName].rightNav;
};

const escapeEmail = (email) => {
  return (email || '').replace('.', ',');
};

export default { isTopNav, isLeftNav, isRightNav, escapeEmail };
