jest.dontMock('react');
jest.dontMock('react-dom/server');
jest.dontMock('../localized');

const React = require('react');
const ReactDOM = require('react-dom/server');

describe("componentWillMount", function () {
  it("should load localization data on first use", function () {

    let localized = require('../localized');
    let store = require('../../stores/LocalizationStore');
    store.isReady = false;

    @localized()
    class TestComponent extends React.Component {}

    let instance = new TestComponent();
    instance.componentWillMount();
    expect(store.load).toBeCalled();

  });
});

describe("bindTranslationHandler", function () {
  it("should expose a `t` helper method on the target component", function () {

    let translate = jest.genMockFn();
    let localized = require('../localized');

    @localized()
    class TestComponent extends React.Component {
      render() {
        expect(this.t).toBeDefined();
        expect(this.t('test')).toBe('localized test');
        return <div />;
      }
    }

    ReactDOM.renderToString(React.createElement(TestComponent));

  });
});

describe("render", function () {
  it("should render normally if store is ready", function () {

    let localized = require('../localized');
    let store = require('../../stores/LocalizationStore');

    @localized()
    class TestComponent extends React.Component {}

    let instance = new TestComponent();
    let rendered = instance.render();
    expect(rendered.type).not.toBe('div');

  });
  it("should render and empty div if store is not ready", function () {

    let localized = require('../localized');
    let store = require('../../stores/LocalizationStore');
    store.isReady = false;

    @localized()
    class TestComponent extends React.Component {}

    let instance = new TestComponent();
    let rendered = instance.render();
    expect(rendered.type).toBe('div');

  });
});
