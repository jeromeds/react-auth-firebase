import React, { Component } from 'react';

/**
 * A very simple component wrapper to be used as a resusable layout
 * @param  {Component} The component to use as the layout
 * @return {Component} The layout component wrapping the target component
 */
export default function layout(component) {
  return function (DecoratedComponent) {
    return class LayoutComponent extends Component {
      static displayName = `layout`;
      render() {
        let content = React.createElement(DecoratedComponent, this.props);
        return React.createElement(component, this.props, content);
      }
    };
  };
}
