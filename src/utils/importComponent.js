import React, { Component, PropTypes } from 'react';

/**
 * Loads a React component on-demand, as a module import
 * @param  {string} path  The path of the component module to load (must be an absolute URL)
 * @return {Component}    The React component proxy, which will load the component asynchronously
 */
export default function importComponent(path) {
	return React.createClass({

		displayName: `import("${path}")`,

		getInitialState() {
			return { component: null };
		},

    componentWillMount() {
			if (!this.state.component) {
				this.loadComponent((component) => {
					if (this.isMounted()) {
						this.setState({ component });
					}
				});
			}
		},

    loadComponent(callback) {
			if (!this.state.component) {
        System.import(path).then((component) => {
          callback && callback(component);
        });
			} else {
				callback && callback(this.state.component);
			}
		},

		render() {
			var component = this.state.component;
			if (component) {
				return React.createElement(component, this.props, this.props.children);
			} else if (this.renderUnavailable) {
				return this.renderUnavailable();
			} else {
				return null;
			}
		}

	});
};
