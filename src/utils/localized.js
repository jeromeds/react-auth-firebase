import React, { Component } from 'react';
import store from '../stores/LocalizationStore.js';

/**
 * Provides a React component with localization capabilities
 * @param  {string} [path]  The path of the localization data to use as root
 * @return {Component}      The wrapped component for which localization props will be passed
 */
export default function localized(path) {
  return function (DecoratedComponent) {
    return class LocalizedComponent extends Component {

      static displayName = `localized("${path}")`;

      constructor(props) {
        super(props);
        this.handleLocalizationReady = this.handleLocalizationReady.bind(this);
        this.state = { localization: this.getLocalization(path) };
        this.bindTranslationHandler(path);
      }

      componentWillMount() {
        store.addChangeListener(this.handleLocalizationReady);
        if (!store.isReady) {
          store.load();
        }
      }

      componentWillUnmount() {
        store.removeChangeListener(this.handleLocalizationReady);
      }

      bindTranslationHandler(path) {
        return DecoratedComponent.prototype.t =
          store.translationHandler(path);
      }

      getLocalization(path) {
        return store.isReady ?
          store.get(path) :
          null;
      }

      handleLocalizationReady() {
        this.bindTranslationHandler(path);
        this.setState({ localization: this.getLocalization(path) });
      }

      render() {
        const { localization } = this.state;
        return localization ?
          <DecoratedComponent
            {...this.props}
            localization={localization}
          /> : <div />;
      }
    };
  };
}
