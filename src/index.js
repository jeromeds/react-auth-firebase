import React from 'react';
import ReactDOM from 'react-dom';
import { createHistory, createHashHistory } from 'history';
import App from './app.js';

// the root element where to mount the application
const mountRoot = document.getElementById('liveshout-app') || document.body;

// use HTML5 history API, but allow hash history via configuration
const history = CONFIG.history === 'hash' ?
  createHashHistory() :
  createHistory();

// mount the application
ReactDOM.render(<App history={history} />, mountRoot);
