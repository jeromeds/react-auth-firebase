import { Dispatcher } from 'flux';

const flux = new Dispatcher();

/**
 * Registers an action handler
 * @param  {string}   [type]    The action type, when handling a specific type
 * @param  {Function} callback  The action handler
 * @return {Object}             The dispatch token
 */
export function register(type, callback) {
  if (type && typeof callback === 'function') {
    return flux.register(action => {
      action.type === type && callback(action);
    });
  }
  return flux.register(type);
}

/**
 * Unregisters a previously registered action handler
 * @param  {Object} id  The dispatch token
 */
export function unregister(id) {
  return flux.unregister(id);
}

/**
 * Wait for other handlers
 * @param  {[Object]} ids  The dispatch tokens to wait for synchronously
 */
export function waitFor(ids) {
  return flux.waitFor(ids);
}

export function isDispatching() {
  return flux.isDispatching();
}

/**
 * Dispatches a single action
 * @param  {string} type      The action type to dispatch
 * @param  {Object} action={} The action payload
 */
export function dispatch(type, action = {}) {
  if (!type) throw new Error('dispatch `type` is required');
  if (action && action.type) throw new Error('dispatched action should not have a `type` property');

  if (CONFIG.debug) {
    if (action && action.error) {
      console.error(type, action);
    } else {
      console.log(type, action);
    }
  }

  return flux.dispatch({ type, ...action });
}

/**
 * Dispatches three actions for an async operation represented by promise.
 * @param  {Promise} promise   The promise representing the asynchronous action
 * @param  {Object}  types     The action types representing `request`, `success` and `failure`
 * @param  {Object}  action    The action payload
 * @return {Promise}           The handled promise
 */
export function dispatchAsync(promise, types, action = {}) {
  const { request, success, failure } = types;
  request && dispatch(request, action);
  return promise.then(
    response => success && dispatch(success, { ...action, response }),
    error => failure && dispatch(failure, { ...action, error })
  );
}

/**
 * Dipatches three actions for an async operation represented by a callback that may be invoked multiple times.
 * @param  {Function} fn       The function accepting a callback to be invoked
 * @param  {Object}   types    The action types representing `request`, `success` and `failure`
 * @param  {Object}   action   The action payload
 * @return {Object}            The value returned from the initial callback invoke
 */
export function dispatchCallback(fn, types, action = {}) {
  const { request, success, failure } = types;
  request && dispatch(request, action);
  return fn((err, response) => {
    if (err) {
      failure && dispatch(failure, { ...action, error: err });
      return;
    }
    success && dispatch(success, { ...action, response });
  });
}
